package main

import (
	"bufio"
	"bytes"
	"errors"
	"flag"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"
)

// https://www.youtube.com/watch?v=CF9S4QZuV30
//https://blog.golang.org/json-and-go

// export PATH=$PATH:/home/bas/go/bin
// git push --all -u --set-upstream git@gitlab.com:heerschop/dcsh.git
// test
//https://www.reddit.com/r/golang/comments/3huv89/ask_rgolang_a_way_to_prevent_flag_from_printing/
//./uconfig.sh "Key|Bash|Dark|Naut" --apply
//./proton ./proton-config.sh --name "Key|Bash|Dark|Naut|Term" --apply
//            "args": ["test-script-01.sh","--show","elements"],
//
// GOARCH=arm64 GOOS=darwin CGO_ENABLED=0 go build
// env GOOS=darwin GOARCH=amd64 go build -v

const protonVersion string = "2.0.0"
const logFileName = "proton-config.log"

type arguments struct {
	Filter         string
	Show           string
	Files          []string
	Apply          bool
	Remove         bool
	Force          bool
	Latent         bool
	Skipped        bool
	Help           bool
	Version        bool
	Init           string
	DisableSpinner bool
	DisableAnsi    bool
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func main() {
	totalStartTime := time.Now()

	enableAnsiSupport()

	var actionIntend actionIntend

	actionIntend = actionDetect

	arguments, usage, err := parseFlags(os.Args[1:])

	if arguments.Help {
		fmt.Printf(usage)
		os.Exit(0)
	}

	if arguments.Version {
		fmt.Println(protonVersion)
		os.Exit(0)
	}

	if arguments.Init == "simple" {
		fmt.Println(templateSimple)
		os.Exit(0)
	}

	if arguments.Init == "complete" {
		fmt.Println(templateComplete)
		os.Exit(0)
	}

	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	if arguments.DisableAnsi {
		disableAnsi()
	}

	if arguments.Apply {
		if arguments.Force {
			actionIntend = actionForce
		} else {
			actionIntend = actionApply
		}
	}

	schema := loadSchema(configSchema)

	if arguments.Show == "schema" {
		printSchema(schema, 0)
		os.Exit(0)
	}

	fmt.Printf(ansiBold+"Loading config files:"+ansiNormal+" %s%s%s\n", ansiDim, arguments.Files, ansiNormal)
	fmt.Println()

	if len(arguments.Files) > 1 {
		fmt.Fprintln(os.Stderr, "Multiple configuration files not yet supported.")
		os.Exit(1)
	}

	lines, err := readFiles(arguments.Files)

	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	fullName, err := filepath.Abs(arguments.Files[0])

	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	scriptRoot := strings.TrimRight(filepath.Dir(fullName), string(os.PathSeparator))

	addElementContent(globalTemplates, keyValue{
		key:   "SCRIPT_ROOT",
		value: scriptRoot,
	})

	tokenChan := startAnalyzer(lines)

	if arguments.Show == "analyze" {
		printAnalyzeResult(tokenChan, arguments.Filter)
		os.Exit(0)
	}

	elementTree, err := tokenParser(tokenChan, schema)

	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	if arguments.Filter != "" {
		fmt.Printf(ansiBold+"Filter:"+ansiNormal+" %s\n", ansiDim+arguments.Filter+ansiNormal)
		fmt.Println()
	}

	if arguments.Show == "elements" {
		printElements(elementTree, false, arguments.Filter)
		os.Exit(0)
	}

	if arguments.Show == "references" {
		printElements(elementTree, true, arguments.Filter)
		os.Exit(0)
	}

	environmentCollector(elementTree, scriptRoot)

	if arguments.Show == "environment" {
		printEnvironment(elementTree, arguments.Filter, scriptRoot)
		os.Exit(0)
	}

	present, init, final, globalSettings, globalTemplates := globalCollector(elementTree, scriptRoot)

	presentScript := actionScript{"present", present.Offset, present.Content}
	initScript := actionScript{"init", init.Offset, init.Content}
	finalScript := actionScript{"final", final.Offset, final.Content}

	if arguments.Show == "" && len(presentScript.script) > 0 {
		tempDir := newTempDir()
		streamPostfix := streamPostfix{
			writer: os.Stdout,
			text:   "\n",
		}

		presentScript.execute(tempDir, &streamPostfix, &streamPostfix, scriptRoot, nil)

		streamPostfix.Done()
		os.RemoveAll(tempDir)
	}

	os.Remove(logFileName)

	log, error := os.Create(logFileName)

	if error != nil {
		fmt.Fprintln(os.Stderr, "Unable to create log file: "+error.Error())
		os.Exit(1)
	}

	outLogger := &logWriter{
		writer: log,
		prefix: "OUT",
	}

	defer log.Close()

	fmt.Fprintf(outLogger, "Filter: %s", arguments.Filter)

	globalEnvironment, nameLength := environmentEvaluator(log, elementTree, scriptRoot, !arguments.DisableSpinner)

	actions := actionCollector(elementTree, scriptRoot)

	if arguments.Show == "actions" {
		fmt.Println()
		printActions(actions, presentScript, initScript, finalScript, globalSettings, globalEnvironment, globalTemplates, arguments.Filter, false)
		os.Exit(0)
	}

	if arguments.Show == "hierarchy" {
		fmt.Println()
		printActions(actions, presentScript, initScript, finalScript, globalSettings, globalEnvironment, globalTemplates, arguments.Filter, true)
		os.Exit(0)
	}

	if arguments.Show != "" {
		fmt.Println()
		fmt.Fprintln(os.Stderr, "Show argument '"+arguments.Show+"' not supported.")
		os.Exit(1)
	}

	showLatent := arguments.Filter != "" || arguments.Latent
	showSkipped := arguments.Filter != "" || arguments.Skipped

	if init.Type == configError {
		fmt.Fprintln(os.Stderr, "Invalid template usage in init script: "+strings.Join(init.Content, "\n"))
		os.Exit(1)
	}

	if final.Type == configError {
		fmt.Fprintln(os.Stderr, "Invalid template usage in final script: "+strings.Join(final.Content, "\n"))
		os.Exit(1)
	}

	actionProcessor(totalStartTime, log, actions, initScript, finalScript, arguments.Filter, scriptRoot, globalEnvironment, arguments.Remove, actionIntend, !arguments.DisableSpinner, arguments.Latent, showSkipped, showLatent, nameLength)
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func readFiles(patterns []string) ([]string, error) {
	var err error

	result := []string{}

	for _, pattern := range patterns {

		files, err := filepath.Glob(pattern)

		if err != nil {
			return nil, err
		}

		for _, file := range files {
			lines, err := readLines(file)

			if err != nil {
				return nil, err
			}

			result = append(result, lines...)
		}
	}

	if len(result) == 0 {
		err = errors.New("No configuration loaded, files not found")
	}

	return result, err
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func readLines(path string) ([]string, error) {
	file, error := os.Open(path)

	if error != nil {
		return nil, error
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	return lines, scanner.Err()
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func parseFlags(args []string) (arguments, string, error) {
	flagSet := &flag.FlagSet{}

	arguments := arguments{
		Filter:         "",
		Show:           "",
		Files:          []string{},
		Apply:          false,
		Remove:         false,
		Force:          false,
		Latent:         false,
		Skipped:        false,
		Help:           false,
		Init:           "",
		Version:        false,
		DisableSpinner: false,
		DisableAnsi:    false,
	}

	flagSet.StringVar(&arguments.Filter, "filter", arguments.Filter, "Filter for specific actions. Expression matching any part of the fully qualified configuration name.")
	flagSet.StringVar(&arguments.Show, "show", arguments.Show, "Show\n\tschema      : Show the schema of the proton config.\n\telements    : Show the elements of the proton config. (references and templates are not expanded)\n\treferences  : Show the elements and expanded references of the proton config. (templates are not expanded)\n\tenvironment : Show non expanded environment values.\n\tactions     : Show action scripts with expanded templates.\n\thierarchy   : Show all action related information.")
	flagSet.BoolVar(&arguments.Apply, "apply", arguments.Apply, "Apply the supplied configuration")
	flagSet.BoolVar(&arguments.Remove, "remove", arguments.Remove, "Execute the remove actions of the supplied configuration.")
	flagSet.BoolVar(&arguments.Force, "force", arguments.Force, "Force, ignore the detect method and directly run apply or remove (disable idempotency)")
	flagSet.BoolVar(&arguments.Latent, "latent", arguments.Latent, "Proccess latent actions.")
	flagSet.BoolVar(&arguments.Skipped, "skipped", arguments.Skipped, "Show skipped actions.")
	flagSet.BoolVar(&arguments.Help, "help", arguments.Help, "Help")
	flagSet.BoolVar(&arguments.Version, "version", arguments.Version, "Show version info")
	flagSet.StringVar(&arguments.Init, "init", arguments.Init, "Init\n\tsimple    : Show a simple starter template containing the basic features of proton.\n\tcomplete  : Show a complete starter template containing all the features of proton.")
	flagSet.BoolVar(&arguments.DisableSpinner, "disableSpinner", arguments.DisableSpinner, "Disable spinner")
	flagSet.BoolVar(&arguments.DisableAnsi, "disableAnsi", arguments.DisableAnsi, "Disable ansi output")

	buffer := bytes.NewBuffer([]byte{})

	flagSet.SetOutput(buffer)

	index := 0

	for ; index < len(args); index++ {
		item := args[index]
		if strings.HasPrefix(item, "-") {
			break
		}

		arguments.Files = append(arguments.Files, item)
	}

	err := flagSet.Parse(args[index:])

	buffer.Reset()

	if index < len(args) {
		arguments.Files = append(arguments.Files, flagSet.Args()...)
	}

	if len(arguments.Files) == 0 && arguments.Show != "schema" {
		err = errors.New("No configuration file specified")
	}

	program := path.Base(os.Args[0])

	fmt.Fprintf(buffer, "Usage: %s [OPTIONS]... [FILES]...\n", program)
	fmt.Fprintf(buffer, "\n")
	fmt.Fprintf(buffer, "Options:\n")

	flagSet.PrintDefaults()

	fmt.Fprintf(buffer, "\n")
	fmt.Fprintf(buffer, "Examples:\n")
	fmt.Fprintf(buffer, "  %s --init simple > config.sh\n", program)
	fmt.Fprintf(buffer, "\n")
	fmt.Fprintf(buffer, "  %s config.sh\n", program)
	fmt.Fprintf(buffer, "  %s config.sh --filter \".*\"\n", program)
	fmt.Fprintf(buffer, "  %s config.sh --apply\n", program)

	usage := string(buffer.Bytes())

	return arguments, usage, err
}
