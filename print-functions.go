package main

import (
	"fmt"
	"reflect"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

var schemaKeywords = []string{
	"remove",
	"apply",
	"detect",
	"term",
	"final",
	"init",
	"environment",
	"templates",
	"settings",
	"present",
	"component",
	"latent",
	"SHELL",
}

var contentPrefix = ansiDim + "- " + ansiNormal + ansiItalic

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func printAnalyzeResult(tokenChan chan elementToken, filter string) {
	filterRegexp := regexp.MustCompile("(?i)" + filter)

	nameColors := map[tokenType]string{
		tokenElementBlock: ansiGreen + ansiBold,
		tokenKeywordBlock: ansiBold,
		tokenCloseBlock:   ansiNormal,
		tokenElementValue: ansiGreen,
		tokenKeywordValue: ansiBold,
		tokenElementLink:  ansiGreen + ansiBold,
		tokenContent:      ansiRed,
		tokenTag:          ansiOrange + ansiBold,
		tokenComment:      ansiRed,
		tokenEmpty:        ansiRed,
		tokenInvalid:      ansiRed,
	}

	valueColors := map[tokenType]string{
		tokenElementBlock: ansiRed,
		tokenKeywordBlock: ansiRed,
		tokenCloseBlock:   ansiRed,
		tokenElementValue: ansiNormal,
		tokenKeywordValue: ansiNormal,
		tokenElementLink:  ansiBlue + ansiBold,
		tokenContent:      ansiItalic,
		tokenTag:          ansiRed,
		tokenComment:      ansiDim,
		tokenEmpty:        ansiRed,
		tokenInvalid:      ansiRed + ansiBold,
	}

	for token := range tokenChan {
		name := tokenNames[token.Type]
		defaultColor := ansiNormal + ansiDim + ansiItalic

		var line = replaceSpaces(token.Raw)

		line = defaultColor + line + ansiNormal

		var nameColor = ansiNormal + nameColors[token.Type]
		var valueColor = ansiNormal + valueColors[token.Type]

		line = strings.Replace(line, token.Name, nameColor+token.Name+defaultColor, 1)

		tokenValue := strings.TrimSpace(token.Value)
		index := strings.LastIndex(line, tokenValue)

		if index != -1 {
			left := line[0:index]
			right := line[index+len(tokenValue):]

			line = left + valueColor + tokenValue + defaultColor + right
		}

		if filterRegexp.MatchString(fmt.Sprintf("%4d : %-14s  %s\n", token.Line+1, name, line)) {
			fmt.Printf("%s%4d%s : %s%-14s  %s\n", ansiDim, token.Line+1, ansiNormal, ansiBold, name, line+ansiNormal)
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func printElements(elementTree []*configRecord, resolver bool, filter string) {
	expression := regexp.MustCompile("(?i)" + filter)

	previousDepth := -1

	elementWalker(elementTree, func(element *configRecord, context *elementContext) (bool, bool) {
		if element.Type == configLink && resolver {
			resolveElementLink(element, elementTree, nil)
		}

		if !element.match(expression, context.name) {
			return false, false
		}

		if previousDepth >= context.depth {
			fmt.Println()
		}

		element.print(context.depth)

		if contains([]string{"present", "init", "final"}, element.Name) && context.depth == 0 {
			fmt.Println()
		}
		if contains([]string{"term"}, element.Name) && context.depth == 1 {
			fmt.Println()
		}

		previousDepth = context.depth - 1

		return true, false
	})
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func printEnvironment(elementTree []*configRecord, filter string, scriptRoot string) {
	contendCount := 0

	contentPrefix := ""

	expression := regexp.MustCompile("(?i)" + filter)

	elementWalker(elementTree, func(element *configRecord, context *elementContext) (bool, bool) {
		if context.data == nil {
			context.data = environmentContext{
				Environment: mergeElementChildren(nil, elementTree, "environment", nil),
				Settings:    mergeElementChildren(nil, elementTree, "settings", nil),
				Templates:   mergeElementChildren(globalTemplates, elementTree, "templates", nil),
			}
		}

		if element.Type != configElementBlock {
			return false, false
		}

		environment := context.data.(environmentContext)
		environment.Name = joinStrings([]string{environment.Name, element.Name}, ".")
		environment.Environment = mergeElementChildren(environment.Environment, element.Children, "environment", nestedembers)
		environment.Settings = mergeElementChildren(environment.Settings, element.Children, "settings", nestedembers)
		environment.Templates = mergeElementChildren(environment.Templates, element.Children, "templates", nestedembers)
		environment.Terms = appendActionScript(environment.Terms, environment.getActionScript("term", element.Children, scriptRoot, environment.Settings, environment.Templates))
		context.data = environment

		if context.depth != 1 {
			return true, false
		}

		fullName := joinStrings([]string{environment.Name, "environment"}, ".")

		if memberMatch(expression, environment.Environment, fullName) {
			fmt.Printf(getNumberPrefix(false, detach(element.Line))+"%s'%s'%s: \n", ansiGreen+ansiBold, environment.Name, ansiNormal)

			for _, term := range environment.Terms {
				printContentBlock(false, term.offset, ansiBold+"term", term.script, 4, contentPrefix)
			}

			if len(environment.Terms) > 0 {
				fmt.Println()
			}

			printChildContent(false, ansiBold+"environment", environment.Environment, 4, contentPrefix, fullName, expression)

			fmt.Println()

			contendCount++
		}

		return false, false

	})

	if contendCount == 0 {
		printNothingToDoMessage("Nothing to show, no environment fields available or selected.", logFileName)
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (action actionConfig) match(expression *regexp.Regexp, fullName string, hierarchy bool) bool {

	if hierarchy {
		if memberMatch(expression, action.environment, joinStrings([]string{fullName, "environment"}, ".")) {
			return true
		}
		if memberMatch(expression, action.templates, joinStrings([]string{fullName, "templates"}, ".")) {
			return true
		}
		if memberMatch(expression, action.settings, joinStrings([]string{fullName, "settings"}, ".")) {
			return true
		}
	}

	if expression.MatchString(fullName) {
		return true
	}

	if expression.MatchString(joinStrings([]string{fullName, "term"}, ".")) && len(action.terms) > 0 {
		return true
	}
	if expression.MatchString(joinStrings([]string{fullName, "detect"}, ".")) && len(action.detect.script) > 0 {
		return true
	}
	if expression.MatchString(joinStrings([]string{fullName, "apply"}, ".")) && len(action.apply.script) > 0 {
		return true
	}
	if expression.MatchString(joinStrings([]string{fullName, "remove"}, ".")) && len(action.remove.script) > 0 {
		return true
	}

	if expression.MatchString(joinStrings([]string{fullName, "component"}, ".")) && action.component.value {
		return true
	}

	return false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func detach(value int) *int {
	return &value
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func getNumberPrefix(hideNumber bool, lineNumber *int) string {
	if hideNumber {
		return ""
	}
	numberPrefix := fmt.Sprintf("%s%4d%s : ", ansiDim, *lineNumber, ansiNormal)
	*lineNumber++

	return numberPrefix
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func printActions(actions []actionConfig, present actionScript, init actionScript, final actionScript, globalSettings map[string]map[string]configContent, globalEnvironment map[string]map[string]configContent, globalTemplates map[string]map[string]configContent, filter string, hierarchy bool) {
	expression := regexp.MustCompile("(?i)" + filter)

	contentPrefix := ""

	contendCount := 0

	errorPrefix := "       "

	if hierarchy {
		errorPrefix = ""
	}

	if hierarchy {
		if printChildContent(true, ansiBold+"settings", globalSettings, 0, contentPrefix, "settings", expression) {
			fmt.Println()
			contendCount++
		}
		if printChildContent(true, ansiBold+"templates", globalTemplates, 0, contentPrefix, "templates", expression) {
			fmt.Println()
			contendCount++
		}
		if printChildContent(true, ansiBold+"environment", globalEnvironment, 0, contentPrefix, "environment", expression) {
			fmt.Println()
			contendCount++
		}
	}

	if expression.MatchString("present") && present.offset >= 0 {
		printContentBlock(hierarchy, present.offset, ansiBold+"present", present.script, 0, contentPrefix)
		fmt.Println()
		contendCount++
	}

	if expression.MatchString("init") && init.offset >= 0 {
		printContentBlock(hierarchy, init.offset, ansiBold+"init", init.script, 0, contentPrefix)
		fmt.Println()
		contendCount++
	}

	if expression.MatchString("final") && final.offset >= 0 {
		printContentBlock(hierarchy, final.offset, ansiBold+"final", final.script, 0, contentPrefix)
		fmt.Println()
		contendCount++
	}

	for _, action := range actions {

		if !action.match(expression, action.name, hierarchy) {
			continue
		}

		fmt.Printf(getNumberPrefix(hierarchy, detach(action.offset))+"%s'%s'%s:\n", ansiGreen+ansiBold, action.name, ansiNormal)
		contendCount++

		if action.error == "" {

			if action.latent.value {
				fmt.Printf(getNumberPrefix(hierarchy, detach(action.latent.offset))+"    %slatent%s\n", ansiOrange+ansiBold, ansiNormal)
			}

			if action.component.value {
				fmt.Printf(getNumberPrefix(hierarchy, detach(action.component.offset))+"    %scomponent%s\n", ansiOrange+ansiBold, ansiNormal)
			}

			if hierarchy {
				if printChildContent(true, ansiBold+"settings", action.settings, 4, contentPrefix, joinStrings([]string{action.name, "settings"}, "."), expression) {
					fmt.Println()
				}
				if printChildContent(true, ansiBold+"templates", action.templates, 4, contentPrefix, joinStrings([]string{action.name, "templates"}, "."), expression) {
					fmt.Println()
				}
				if printChildContent(true, ansiBold+"environment", action.environment, 4, contentPrefix, joinStrings([]string{action.name, "environment"}, "."), expression) {
					fmt.Println()
				}
			}

			if expression.MatchString(joinStrings([]string{action.name, "term"}, ".")) && len(action.terms) > 0 {
				for _, term := range action.terms {
					printContentBlock(hierarchy, term.offset, ansiBold+"term", term.script, 4, contentPrefix)
				}
				fmt.Println()
			}

			if expression.MatchString(joinStrings([]string{action.name, "detect"}, ".")) && len(action.detect.script) > 0 {
				printContentBlock(hierarchy, action.detect.offset, ansiBold+"detect", action.detect.script, 4, contentPrefix)
				fmt.Println()
			}

			if expression.MatchString(joinStrings([]string{action.name, "apply"}, ".")) && len(action.detect.script) > 0 {
				printContentBlock(hierarchy, action.apply.offset, ansiBold+"apply", action.apply.script, 4, contentPrefix)
				fmt.Println()
			}
			if expression.MatchString(joinStrings([]string{action.name, "remove"}, ".")) && len(action.detect.script) > 0 {
				printContentBlock(hierarchy, action.remove.offset, ansiBold+"remove", action.remove.script, 4, contentPrefix)
			}
		} else {
			fmt.Printf(errorPrefix+"    %sERROR: %s%s\n", ansiRed+ansiBold, action.error, ansiNormal)
		}

		fmt.Println()
	}

	if contendCount == 0 {
		printNothingToDoMessage("Nothing to do, no actions available or selected.", logFileName)
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func printSchema(schema schemaElement, indent int) {
	indentText := strings.Repeat(" ", indent*4)

	fmt.Printf("%sContent: %s\n", indentText, ansiDim+strconv.FormatBool(schema.Content)+ansiNormal)

	keys := getSortedKeys(schema.Children)

	for _, key := range keys {
		value := schema.Children[key]
		ansiStyle := ansiBold
		tag := false
		enclose := ""

		if strings.HasPrefix(key, "^") && strings.HasSuffix(key, "$") {
			if indent == 1 {
				fmt.Println()
			}
			ansiStyle = ansiGreen
			enclose = "'"
		} else if !value.Content && len(value.Children) == 0 {
			ansiStyle = ansiOrange + ansiBold
			tag = true
		}
		if indent == 0 {
			fmt.Println()
		}

		fmt.Printf("%s%s%s%s", indentText, ansiStyle, enclose+key+enclose, ansiNormal)

		if tag {
			fmt.Println()
		} else {
			fmt.Printf(":\n")
			printSchema(value, indent+1)
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func printFooter(actionText map[actionResult]*actionText) {

	if actionText[resultDesired].count == 0 {
		actionText[resultDesired].color = ansiDim
	}
	if actionText[resultUpdated].count == 0 {
		actionText[resultUpdated].color = ansiDim
	}
	if actionText[resultRemoved].count == 0 {
		actionText[resultRemoved].color = ansiDim
	}

	if actionText[resultMissing].count == 0 {
		actionText[resultMissing].color = ansiDim
	}
	if actionText[resultSkipped].count == 0 {
		actionText[resultSkipped].color = ansiDim
	}
	if actionText[resultLatent].count == 0 {
		actionText[resultLatent].color = ansiDim
	}

	if actionText[resultDestroy].count == 0 {
		actionText[resultDestroy].color = ansiDim
	}
	if actionText[resultFailed].count == 0 {
		actionText[resultFailed].color = ansiDim
	}
	if actionText[resultComponent].count == 0 {
		actionText[resultComponent].color = ansiDim
	}
	if actionText[resultFiltered].count == 0 {
		actionText[resultFiltered].color = ansiDim
	}

	fmt.Printf(ansiItalic+"Desired : %s%3d%s    Updated : %s%3d%s    Destroy : %s%3d%s    Skipped : %s%3d%s    Component : %s%3d%s\n", actionText[resultDesired].color+ansiItalic, actionText[resultDesired].count, ansiNormal+ansiItalic, actionText[resultUpdated].color+ansiItalic, actionText[resultUpdated].count, ansiNormal+ansiItalic, actionText[resultDestroy].color+ansiItalic, actionText[resultDestroy].count, ansiNormal+ansiItalic, actionText[resultSkipped].color+ansiItalic, actionText[resultSkipped].count, ansiNormal+ansiItalic, actionText[resultComponent].color+ansiItalic, actionText[resultComponent].count, ansiNormal+ansiItalic)
	fmt.Printf(ansiItalic+"Missing : %s%3d%s    Failed  : %s%3d%s    Removed : %s%3d%s    Latent  : %s%3d%s    Filtered  : %s%3d%s\n", actionText[resultMissing].color+ansiItalic, actionText[resultMissing].count, ansiNormal+ansiItalic, actionText[resultFailed].color+ansiItalic, actionText[resultFailed].count, ansiNormal+ansiItalic, actionText[resultRemoved].color+ansiItalic, actionText[resultRemoved].count, ansiNormal+ansiItalic, actionText[resultLatent].color+ansiItalic, actionText[resultLatent].count, ansiNormal+ansiItalic, actionText[resultFiltered].color+ansiItalic, actionText[resultFiltered].count, ansiNormal+ansiItalic)

}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func printContentBlock(hideNumbers bool, lineNumber int, name string, content []string, indent int, prefix string) {
	indents := strings.Repeat(" ", indent)

	if len(content) == 0 {
		fmt.Printf(getNumberPrefix(hideNumbers, &lineNumber)+indents+"%s:\n", name+ansiNormal)
		return
	}

	fmt.Printf(getNumberPrefix(hideNumbers, &lineNumber)+indents+"%s:\n", name+ansiNormal)
	for _, line := range content {
		if strings.HasPrefix(line, "#!") {
			fmt.Printf(getNumberPrefix(hideNumbers, &lineNumber)+indents+"    %s\n", prefix+ansiDim+line+ansiNormal)
			continue
		}

		fmt.Printf(getNumberPrefix(hideNumbers, &lineNumber)+indents+"    %s\n", prefix+line+ansiNormal)
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func printChildContent(hideNumbers bool, name string, childContent map[string]map[string]configContent, indent int, prefix string, fullName string, expression *regexp.Regexp) bool {
	if !memberMatch(expression, childContent, fullName) {
		return false
	}

	nameList := getSortedKeys(childContent)
	nameList, _ = remove(nameList, "_top_")

	indentPrefix := ansiDim + "  ??" + ansiNormal + " :" + strings.Repeat(" ", indent)

	if hideNumbers {
		indentPrefix = strings.Repeat(" ", indent)
	}

	if !hasChildren(childContent) {
		fmt.Printf(indentPrefix+"%s:\n", name+ansiNormal)
		return true
	}

	fmt.Printf(indentPrefix+"%s:\n", name+ansiNormal)

	for _, name := range nameList {
		child := childContent[name]

		fullName := joinStrings([]string{fullName, name}, ".")

		if len(child) > 0 && match(expression, child, fullName) {
			fmt.Printf(indentPrefix+"    %s%s%s:\n", ansiBold, name, ansiNormal)
			for _, name := range getSortedKeys(child) {
				if expression.MatchString(joinStrings([]string{fullName, name}, ".")) {
					child[name].print(hideNumbers, name, indent+8, prefix)
				}
			}
		}
	}
	return true
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (content configContent) print(hideNumbers bool, name string, indent int, prefix string) {
	indentPrefix := strings.Repeat(" ", indent)

	inheritancePrefix := "??"

	switch content.Inherited {
	case 0:
		inheritancePrefix = "=="
	case 1:
		inheritancePrefix = "=<"
	case 2:
		inheritancePrefix = "<<"
	case 3:
		inheritancePrefix = "<<<"
	}

	if indent == 0 {
		inheritancePrefix = ""
	}

	if contains(schemaKeywords, name) {
		inheritancePrefix = ansiBold + inheritancePrefix
	} else {
		inheritancePrefix = ansiGreen + inheritancePrefix
	}

	switch content.Type {
	case configValue:
		fmt.Printf(getNumberPrefix(hideNumbers, detach(content.Offset))+indentPrefix+"%s%s: %s%s\n", inheritancePrefix+name, ansiNormal, content.Content[0], ansiNormal)
	case configError:
		printContentBlock(hideNumbers, content.Offset, ansiGreen+inheritancePrefix+name, content.Content, indent, ansiRed+ansiBold+"ERROR: ")
	default:
		printContentBlock(hideNumbers, content.Offset, inheritancePrefix+name, content.Content, indent, prefix)
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (element *configRecord) print(indent int) {
	indentPrefix := strings.Repeat(" ", indent*4)

	switch element.Type {
	case configElementBlock:
		if len(element.Content) == 0 {
			if len(element.Children) == 0 {
				fmt.Printf("%s%s'%s'%s:\n", indentPrefix, ansiGreen+ansiBold, element.Name, ansiNormal)
			} else {
				fmt.Printf("%s%s'%s'%s:\n", indentPrefix, ansiGreen+ansiBold, element.Name, ansiNormal)
			}
			return
		}
		printContentBlock(true, 0, ansiGreen+element.Name, element.Content, indent*4, ansiDim+"- "+ansiNormal+ansiItalic)
	case configKeywordBlock:
		if len(element.Content) == 0 {
			if len(element.Children) == 0 {
				fmt.Printf("%s%s%s%s:\n", indentPrefix, ansiBold, element.Name, ansiNormal)
			} else {
				fmt.Printf("%s%s%s%s:\n", indentPrefix, ansiBold, element.Name, ansiNormal)
			}
			return
		}
		printContentBlock(true, 0, ansiBold+element.Name, element.Content, indent*4, ansiDim+"- "+ansiNormal+ansiItalic)
	case configValue:
		if contains(schemaKeywords, element.Name) {
			fmt.Printf("%s%s%s%s: %s%s\n", indentPrefix, ansiBold, element.Name, ansiNormal, element.Content[0], ansiNormal)
		} else {
			fmt.Printf("%s%s'%s'%s: %s%s\n", indentPrefix, ansiGreen, element.Name, ansiNormal, element.Content[0], ansiNormal)
		}
	case configLink:
		fmt.Printf("%s%s'%s'%s:%s <%s>%s\n", indentPrefix, ansiGreen+ansiBold, element.Name, ansiNormal, ansiBlue+ansiBold, element.Content[0], ansiNormal)
	case configTag:
		fmt.Printf("%s%s%s%s\n", indentPrefix, ansiOrange+ansiBold, element.Name, ansiNormal)
	case configError:
		printContentBlock(true, 0, ansiGreen+ansiBold+"'"+element.Name+"'", element.Content, indent*4, ansiRed+ansiBold+"ERROR: ")
	default:
		printContentBlock(true, 0, ansiRed+ansiBold+"ERROR: Element type for '"+element.Name+"' not supported", element.Content, indent*4, ansiRed+ansiBold+"ERROR: ")
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func getSortedKeys(hash interface{}) []string {
	var array []string

	for _, value := range reflect.ValueOf(hash).MapKeys() {
		array = append(array, value.String())
	}

	sort.Strings(array)

	for _, item := range schemaKeywords {
		if result, success := remove(array, item); success {
			array = append([]string{item}, result...)
		}
	}

	return array
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func hasChildren(nestedMap map[string]map[string]configContent) bool {
	for _, value := range nestedMap {
		if value != nil && len(value) > 0 {
			return true
		}
	}
	return false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func remove(array []string, item string) ([]string, bool) {
	for index, value := range array {
		if value == item {
			return append(array[:index], array[index+1:]...), true
		}
	}
	return array, false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func contains(array []string, value string) bool {
	for _, item := range array {
		if value == item {
			return true
		}
	}
	return false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (element *configRecord) match(expression *regexp.Regexp, fullName string) bool {
	if expression.MatchString(fullName) {
		return true
	}

	for _, element := range element.Children {
		if element.match(expression, joinStrings([]string{fullName, element.Name}, ".")) {
			return true
		}
	}

	return false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func match(expression *regexp.Regexp, elementChildren map[string]configContent, fullName string) bool {
	for name, _ := range elementChildren {

		fullName := joinStrings([]string{fullName, name}, ".")
		if expression.MatchString(fullName) {
			return true
		}
	}

	return false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func memberMatch(expression *regexp.Regexp, elementChildren map[string]map[string]configContent, fullName string) bool {
	for name, element := range elementChildren {
		if name == "_top_" {
			continue
		}
		fullName := joinStrings([]string{fullName, name}, ".")
		if match(expression, element, fullName) {
			return true
		}
	}

	return false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func replaceSpaces(line string) string {
	result := strings.TrimLeft(line, " \t")
	subString := line[0 : len(line)-len(result)]

	subString = strings.Replace(subString, " ", ".", -1)
	subString = strings.Replace(subString, "\t", " ➜  ", -1)

	return subString + result
}
