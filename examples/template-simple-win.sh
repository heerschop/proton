#!proton

# This simple example demonstrates the basic functionality of
# proton by creating a single text file.

# To create this template
# run: proton --init simple > template-simple.sh

# To show the resulting actions scripts
# run: proton --show actions

settings:
    SHELL: "powershell !.ps1"

templates:
    init:
        $ErrorActionPreference=[Management.Automation.ActionPreference]::Stop

"My Category":
    "My Configuration":
        detect:
            Write-Host 'Script to detect the file.'
            $result=Test-Path 'proton-example'
            [Environment]::Exit(!$result)
        
        apply:
            Write-Host 'Script to create the file.'
            Write-Output "hello proton" | Out-File 'proton-example'
        
        remove:
            Write-Host 'Script to remove the file.'
            Remove-Item 'proton-example'
