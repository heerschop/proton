#!proton

# settings
# template
# environment
# init
# final
# component
# latent
# term
# detect
# apply
# remove

present:
    echo Visual Studio Code configuration.

settings:

templates:
    "IsWindows()":
        #!proton-expression
        "$GOOS"=="windows"
    "IsMacOS()":
        #!proton-expression
        "$GOOS"=="darwin"
    "IsLinux()":
        #!proton-expression
        "$GOOS"=="linux"

"Linux":
    term:
        $IsLinux()
    settings:
        SHELL: "/bin/bash"

    environment:
        "TEMP_PATH": "/tmp"

    templates:
        init:
            set -e
        term:
            init: ""                                       # Empty the init template for the category term
        "IsRoot()":
            [[ $EUID == 0 ]] || (echo "Should be run as root" && exit 1)
        "IsUser()":
            [[ $EUID != 0 ]] || (echo "Should be run as user" && exit 1)

    "Visual Studio Code":
        term:
            $IsRoot()
        detect:
            dpkg -s code | grep "Status: install ok installed"
        apply:
            wget -nv -L https://go.microsoft.com/fwlink/?LinkID=760868 -O "$TEMP_PATH/vscode.deb"
            apt-get install --yes "$TEMP_PATH/vscode.deb"

"Macos":
    term:
        $IsMacOS()

"Windows":
    term:
        $IsWindows()
    settings:
        SHELL: "powershell !.ps1"

    environment:
        "TEMP_PATH": "$TEMP"

    templates:
        init:
            $ErrorActionPreference=[Management.Automation.ActionPreference]::Stop
            $ProgressPreference=[Management.Automation.ActionPreference]::SilentlyContinue
        term:
            init: ""                                       # Empty the init template for the category term
        "IsRoot()":
            #Requires -RunAsAdministrator
            Write-Host 'Running as admin!'

    "NuGet":
        term:
            $IsRoot()
        templates:
            remove:
                init: ""
        environment:
             "NUGET_PROVIDER_PATH":
                (Get-PackageProvider NuGet -ListAvailable -ErrorAction Ignore).ProviderPath
        detect:
            try
            {
                $package = Get-PackageProvider NuGet -ListAvailable
                Write-Host $package.Version
            }
            catch 
            {
                Write-Host $_
                [Environment]::Exit(1)
            }
        apply:
            Install-PackageProvider NuGet -force
        remove:
            #!cmd /q /c !.cmd
            echo Removing: %NUGET_PROVIDER_PATH%
            del "%NUGET_PROVIDER_PATH%"
