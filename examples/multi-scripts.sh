#!proton

"Multi scripts":
    "Bash":
        detect:
            #!/bin/bash
            echo SHELL: $SHELL $SHELL_ARGS
            echo VERSION: $BASH_VERSION
            echo
            echo PARENT: $(ps -p $(ps -p $$ -o ppid=) -o comm=)
            echo SHELL: $(readlink /proc/$$/exe)
        apply:
            #!/bin/bash
            echo "Bash apply stuff..."

    "Python":
        detect:
            #!/usr/bin/env python
            import sys,os

            print("SHELL: " + os.environ['SHELL'] + " " + os.environ['SHELL_ARGS'])
            print("VERSION:" + sys.version)
        apply:
            #!/usr/bin/env python
            print('Node apply stuff...')

    "Node":
        detect:
            #!/usr/bin/env node
            console.log('SHELL: ' + process.env.SHELL + ' ' + process.env.SHELL_ARGS)
            console.log('VERSION: ' + process.version)
        apply:
            #!/usr/bin/env node
            console.log('Node apply stuff...')

    "PowerShell":
        detect:
            #!/usr/bin/env pwsh
            Write-Host "SHELL: $($ENV:SHELL) $($ENV:SHELL_ARGS)"
            Write-Host "VERSION: $($PSVersionTable.PSVersion)"
        apply:
            #!/usr/bin/env pwsh
            Write-Host 'PowerShell apply stuff...'

    "Perl":
        detect:
            #!/usr/bin/env perl
            print "SHELL: $ENV{'SHELL'} $ENV{'SHELL_ARGS'}\n";
            print "VERSION: $]\n";
        apply:
            #!/usr/bin/env perl
            print "Perl apply stuff..."

    "GoLang":
        detect:
            #!go run !.go
            package main
            
            import (
                "fmt"
                "runtime"
                "os"
            )

            func main() {
                fmt.Println("SHELL: " + os.Getenv("SHELL") + " " + os.Getenv("SHELL_ARGS"))
                fmt.Println("VERSION: " + runtime.Version())
            }
        apply:
            #!go run !.go
            package main

            import "fmt"

            func main():
                fmt.Printf("GoLang apply stuff...")
