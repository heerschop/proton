#!proton

present:
    echo Golang $GOLANG_VERSION configuration.

settings:

templates:
    "GOLANG_VERSION": "1.11.4"
    "IsWindows()":
        #!proton-expression
        "$GOOS"=="windows"
    "IsMacOS()":
        #!proton-expression
        "$GOOS"=="darwin"
    "IsLinux()":
        #!proton-expression
        "$GOOS"=="linux"

"Linux":
    term:
        $IsLinux()
    settings:
        SHELL: "/bin/bash"

    environment:
        "TEMP_PATH": "/tmp"
        "GOLANG": "https://dl.google.com/go/go$GOLANG_VERSION.linux-amd64.tar.gz"
    templates:
        init:
            set -e
        term:
            init: ""
        "IsRoot()":
            [[ $EUID == 0 ]] || (echo "Should be run as root" && exit 1)
        "IsUser()":
            [[ $EUID != 0 ]] || (echo "Should be run as user" && exit 1)

    "Golang":
        term:
            $IsRoot()
        detect:
            [ -d /usr/local/go ]
            /usr/local/go/bin/go version
        apply:
            wget -nv $GOLANG -O "$TEMP_PATH/go-linux-amd64.tar.gz"

            tar -C /usr/local -xzf "$TEMP_PATH/go-linux-amd64.tar.gz"
        remove:
            echo remove

"Macos":
    term:
        $IsMacOS()
    settings:
        SHELL: "/bin/bash"

    environment:
        "TEMP_PATH": "/tmp"
        "GOLANG": "https://dl.google.com/go/go$GOLANG_VERSION.darwin-amd64.pkg"
    
    templates:
        init:
            set -e
        term:
            init: ""
        "IsRoot()":
            [[ $EUID == 0 ]] || (echo "Should be run as root" && exit 1)
        "IsUser()":
            [[ $EUID != 0 ]] || (echo "Should be run as user" && exit 1)

    "Golang":
        term:
            $IsRoot()
        detect:
            # pkgutil --files packagename
            [ -d /usr/local/go ]
            /usr/local/go/bin/go version
        apply:
            wget -nv $GOLANG -O "$TEMP_PATH/go-darwin-amd64.pkg"

            echo sudo /usr/sbin/installer -pkg "$TEMP_PATH/go-darwin-amd64.pkg" -target /
        remove:
            echo sudo pkgutil --forget package-name.pkg

"Windows":
    term:
        $IsWindows()
    settings:
        SHELL: "powershell !.ps1"

    environment:
        "TEMP_PATH": "$TEMP"
        "GOLANG": "https://dl.google.com/go/go$GOLANG_VERSION.windows-amd64.msi"

    templates:
        "IsRoot()":
            if ([Security.Principal.WindowsIdentity]::GetCurrent().Groups -notcontains "S-1-5-32-544") {
                Write-Host "Should be run as admin"
                [Environment]::Exit(1)
            }
            [Environment]::Exit(0)

    "GoLang":
        environment:
            "MSI_FILE":
                [IO.Path]::Combine($env:TEMP,"go$GOLANG_VERSION.windows-amd64.msi")
            "LOG_FILE":
                [IO.Path]::Combine($env:TEMP,"go$GOLANG_VERSION.windows-amd64.log")
            detect:
                "GOLANG":
                    if (!$env:GOROOT) { $env:GOROOT=$env:SystemDrive,'Go' -Join [IO.Path]::DirectorySeparatorChar }
                    
                    [IO.Path]::Combine($env:GOROOT,'bin','go.exe')

        templates:
            init:
                $ErrorActionPreference=[Management.Automation.ActionPreference]::Stop
                $ProgressPreference=[Management.Automation.ActionPreference]::SilentlyContinue
               
                if (!(Test-Path $env:MSI_FILE)) { Invoke-WebRequest -Uri $env:GOLANG -OutFile $env:MSI_FILE }
            detect:
                init:
                    $ErrorActionPreference=[Management.Automation.ActionPreference]::Stop
            environment:
                init: ""
            term:
                init: ""

        term:
            $IsRoot()
        detect:
            Start-Process -Wait -NoNewWindow $env:GOLANG -ArgumentList version
        apply:
            $process=Start-Process -PassThru -Wait -FilePath msiexec -ArgumentList /i, $env:MSI_FILE, /qn, /l*v, $env:LOG_FILE
            
            [Environment]::Exit($process.ExitCode)
        remove:
            $process=Start-Process -PassThru -Wait -FilePath msiexec -ArgumentList /x, $env:MSI_FILE, /qn, /l*v, $env:LOG_FILE
            
            [Environment]::Exit($process.ExitCode)
