#!proton

# https://git-scm.com/
# https://golang.org/dl/
# https://code.visualstudio.com/
# https://github.com/derekparker/delve

settings:
    SHELL: "/bin/bash"

environment:
    "TEMP_PATH": "/tmp"
    "GOLANG": "https://dl.google.com/go/go1.11.2.linux-amd64.tar.gz"

templates:
    init:
        set -e
    "IsRoot()":
        [ $EUID == 0 ] || (echo "Should be run as root." && exit 1)
    "IsUser()":
        [ $EUID != 0 ] || (echo "Should be run as user." && exit 1)

"System-Components":
    component
    term:
        $IsRoot()
    "Git":
        detect:
            dpkg -s git | grep "Status: install ok installed"
        apply:
            apt-get --yes install git
    "Golang":
        detect:
            [ -d /usr/local/go ]
        apply:
            wget -nv $GOLANG -O "$TEMP_PATH/go-linux-amd64.tar.gz"

            tar -C /usr/local -xzf "$TEMP_PATH/go-linux-amd64.tar.gz"
    "Visual Studio Code":
        detect:
            dpkg -s code | grep "Status: install ok installed"
        apply:
            wget -nv -L https://go.microsoft.com/fwlink/?LinkID=760868 -O "$TEMP_PATH/vscode.deb"
            apt-get install --yes "$TEMP_PATH/vscode.deb"

"User-Components":
    component
    term:
        $IsUser()
    "Golang Profile":
        detect:
            cat "$HOME/.profile" | grep "PATH=.*/usr/local/go/bin.*"
        apply:
            cat "$HOME/.profile" | grep "PATH=.*/usr/local/go/bin.*" || echo "PATH=\"\$PATH:/usr/local/go/bin\"" >> "$HOME/.profile"
    "Golang Debugger":
        detect:
            [ -f ~/go/bin/dlv ]
        apply:
            /usr/local/go/bin/go get -u github.com/derekparker/delve/cmd/dlv
    "Visual Studio Code":
        detect:
            code  --list-extensions | grep ms-vscode.Go
        apply:
            code --install-extension ms-vscode.Go
        remove:

"Golang":
    "Git": <System-Components.Git>
    "Binary": <System-Components.Golang>
    "Visual Studio Code": <System-Components.Visual Studio Code>

"Golang":
    "Golang Profile": <User-Components.Golang Profile>
    "Golang Debugger": <User-Components.Golang Debugger>
    "Visual Studio Code Extensions": <User-Components.Visual Studio Code>
