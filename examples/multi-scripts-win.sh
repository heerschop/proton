#!proton

"Multi scripts":
    "CMD":
        detect:
            echo SHELL: %SHELL% %SHELL_ARGS%
            ver
        apply:
            echo "CMD apply stuff..."
    "Bash":
        detect:
            #!"$ProgramFiles\Git\bin\bash" !.sh
            echo SHELL: $SHELL $SHELL_ARGS
            echo VERSION: $BASH_VERSION
            echo
            echo PARENT: $(ps -p $(ps -p $$ -o ppid=) -o comm=)
            echo SHELL: $(readlink /proc/$$/exe)
        apply:
            #!"$ProgramFiles\Git\bin\bash" !.sh
            echo "Bash apply stuff..."

    "Python":
        detect:
            #!python !.py
            import sys,os

            print("SHELL: " + os.environ['SHELL'] + " " + os.environ['SHELL_ARGS'])
            print("VERSION:" + sys.version)
        apply:
            #!python !.py
            print('Node apply stuff...')

    "Node":
        detect:
            #!node !.js
            console.log('SHELL: ' + process.env.SHELL + ' ' + process.env.SHELL_ARGS)
            console.log('VERSION: ' + process.version)
        apply:
            #!node !.js
            console.log('Node apply stuff...')

    "PowerShell":
        detect:
            #!powershell !.ps1
            Write-Host "SHELL: $($ENV:SHELL) $($ENV:SHELL_ARGS)"
            Write-Host "VERSION: $($PSVersionTable.PSVersion)"
        apply:
            #!powershell !.ps1
            Write-Host 'PowerShell apply stuff...'

    "Perl":
        detect:
            #!perl !.pl
            print "SHELL: $ENV{'SHELL'} $ENV{'SHELL_ARGS'}\n";
            print "VERSION: $]\n";
        apply:
            #!perl !.pl
            print "Perl apply stuff..."

    "GoLang":
        detect:
            #!go run !.go
            package main
            
            import (
                "fmt"
                "runtime"
                "os"
            )

            func main() {
                fmt.Println("SHELL: " + os.Getenv("SHELL") + " " + os.Getenv("SHELL_ARGS"))
                fmt.Println("VERSION: " + runtime.Version())
            }
        apply:
            #!go run !.go
            package main

            import "fmt"

            func main():
                fmt.Printf("GoLang apply stuff...")
