#!proton

# This simple example demonstrates the basic functionality of 
# proton by creating a single text file.

# To create this template 
# run: proton --init simple > template-simple.sh

# To show the resulting actions scripts
# run: proton --show actions

settings:
    SHELL: "/bin/bash"

templates:
    init:
        set -e

"My Category":
    "My Configuration":
        detect:
            echo Script to detect the file.
            [ -f proton-example ]
        
        apply:
            echo Script to create the file.
            echo "hello proton" >  proton-example
        
        remove:
            echo Script to remove the file.
            rm proton-example
