#!proton

present:
    BOLD="\033[1m"
    NORMAL="\033[0m"

    version="$(. /etc/os-release;echo $NAME $VERSION)  $(gnome-shell --version)  ($(date +%Y-%m-%d\ %H:%M:%S))"
    printf "$BOLD%s$NORMAL\n" "Configuring $version"

settings:
    SHELL: "/bin/bash"

environment:
    "TEMP_PATH": "/tmp/proton"
    "NVM":                    "https://raw.githubusercontent.com/creationix/nvm/v0.35.1/install.sh"
    "DOT_NET_CORE":
        echo "https://packages.microsoft.com/config/ubuntu/$(lsb_release -rs)/packages-microsoft-prod.deb"
    "ANDROID_SDK":            "https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip"
    "GOLANG":                 "https://dl.google.com/go/go1.13.5.linux-amd64.tar.gz"
    "POWER_SHELL_CORE":
        echo "https://packages.microsoft.com/config/ubuntu/$(lsb_release -rs)/packages-microsoft-prod.deb"
    "CLOUD_STATION_DRIVE":    "https://global.download.synology.com/download/Tools/CloudStationDrive/4.3.2-4450/Ubuntu/Installer/x86_64/synology-cloud-station-drive-4450.x86_64.deb"
    "USER_THEMES_EXTENSION":  "https://extensions.gnome.org/download-extension/putWindow@clemens.lab21.org.shell-extension.zip?version_tag=10507"

templates:
    init:
        set -e
    "IsRoot()":
        [ $EUID == 0 ] || (echo "Should be run as root." && exit 1)
    "IsUser()":
        [ $EUID != 0 ] || (echo "Should be run as user." && exit 1)

init:
    mkdir -p $TEMP_PATH

    [[ $EUID == 0 ]] || mkdir -p ~/bin

final:
    rm -rf $TEMP_PATH

"DEV":
    term:
        $IsRoot()

    "Source control tools":
        detect:
            dpkg -s git 2>/dev/null | grep -oP "Version: \K.*"
        apply:
            apt-get install --yes git

    "Zsh":
        detect:
            dpkg -s zsh 2>/dev/null | grep -oP "Version: \K.*"
        apply:
            apt-get install --yes zsh

            chsh -s /bin/zsh $(logname)

    "Build Essential":
        detect:
            dpkg -s build-essential 2>/dev/null | grep -oP "Version: \K.*"
        apply:
            apt-get install --yes build-essential

    "Python Minimal":
        detect:
            dpkg -s python-minimal 2>/dev/null | grep -oP "Version: \K.*"
        apply:
            apt-get install --yes python-minimal

    ".NET Core SDK":
        latent
        detect:
            dpkg -s dotnet-sdk-2.1 2>/dev/null | grep -oP "Version: \K.*"
        apply:
            dpkg -s packages-microsoft-prod || ( # Download and Register the Microsoft repository GPG keys
                wget -q $DOT_NET_CORE -O $TEMP_PATH/packages-microsoft-prod.deb
                dpkg -i $TEMP_PATH/packages-microsoft-prod.deb
            )

            apt-get install --yes apt-transport-https
            apt-get update
            apt-get install --yes dotnet-sdk-2.1

    "PowerShell Core":
        latent
        detect:
            dpkg -s powershell 2>/dev/null | grep -oP "Version: \K.*"
        apply:
            dpkg -s packages-microsoft-prod || ( # Download and Register the Microsoft repository GPG keys
                wget -q $POWER_SHELL_CORE -O $TEMP_PATH/packages-microsoft-prod.deb
                dpkg -i $TEMP_PATH/packages-microsoft-prod.deb
            )

            apt-get update
            apt-get install --yes powershell

    "Golang":
        latent
        detect:
            [ -d /usr/local/go ]
        apply:
            wget -nv $GOLANG -O "$TEMP_PATH/go-linux-amd64.tar.gz"

            tar -C /usr/local -xzf "$TEMP_PATH/go-linux-amd64.tar.gz"

"DEV":
    term:
        $IsUser()

    "SSH Configure":
        detect:
            [ -f ~/.ssh/id_rsa_user@gmail.com ]     && \
            [ -f ~/.ssh/id_rsa_user@gmail.com.pub ]
        apply:
            mkdir -p ~/.ssh
            
            cp $SCRIPT_ROOT/resources/ssh-keys/id_rsa_* ~/.ssh

            chmod 600 ~/.ssh/id_rsa_*
            chmod 644 ~/.ssh/id_rsa_*.pub

            if [ $? != 0 ] ; then
                rm ~/.ssh/id_rsa_*
            fi

    "Git Configure":
        detect:
            git config --global user.name    | grep -q "user@gmail.com"
            git config --global pager.branch | grep -q "false"
            git config --global pager.tag    | grep -q "false"
        apply:
            git config --global user.email "user@gmail.com"
            git config --global pager.branch false
            git config --global pager.tag false

    "Oh My Zsh":
        detect:
            #!/bin/zsh
            [ -d $HOME/.oh-my-zsh ]
        apply:
            #!/bin/zsh
            export SHELL=/bin/zsh

            sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

            cp ~/.profile ~/.zprofile

    "NVM":
        detect:
            #!/bin/zsh
            [ ! $NVM_DIR ] && NVM_DIR=~/.nvm
            [ -f "$NVM_DIR/nvm.sh" ]
            [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh";nvm --version
        apply:
            #!/bin/zsh
            wget -qO- $NVM | zsh

    "Node":
        detect:
            #!/bin/zsh
            [ ! $NVM_DIR ] && NVM_DIR=~/.nvm
            [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh";nvm current | grep -v none
        apply:
            #!/bin/zsh
            [ ! $NVM_DIR ] && NVM_DIR=~/.nvm
            [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"
            nvm install lts/dubnium

    "Golang Debugger":
        latent
        detect:
            cat "$HOME/.profile" | grep "PATH=.*/usr/local/go/bin.*"
        apply:
            set -e

            cat "$HOME/.profile" | grep "PATH=.*/usr/local/go/bin.*" || echo "PATH=\"\$PATH:/usr/local/go/bin\"" >> "$HOME/.profile"

            /usr/local/go/bin/go get github.com/derekparker/delve/cmd/dlv

    "SdkMan":
        latent
        detect:
            [ -d ~/.sdkman ]
        apply:
            curl -s "https://get.sdkman.io" | bash

            source "$HOME/.sdkman/bin/sdkman-init.sh"

    "Java Development Kit (JDK)":
        latent
        detect:
            source ~/.sdkman/bin/sdkman-init.sh

            sdk list java | grep "> \*"
        apply:
            source ~/.sdkman/bin/sdkman-init.sh
            
            sdk install java

    "Android SDK Manager":
        #https://developer.android.com/studio/command-line/sdkmanager.html
        latent
        detect:
            [ -d ~/Android/Sdk ]
        apply:
            wget -nv $ANDROID_SDK  -O $TEMP_PATH/sdk-tools-linux.zip

            mkdir -p ~/Android/Sdk

            unzip $TEMP_PATH/sdk-tools-linux.zip -d ~/Android/Sdk

            source ~/.sdkman/bin/sdkman-init.sh

            sdk install gradle

            export ANDROID_HOME=~/Android/Sdk
            export PATH=$PATH:$ANDROID_HOME/tools/bin

            yes | sdkmanager --licenses

            sdkmanager platform-tools
            sdkmanager "build-tools;28.0.1"

"SHELL":
    term:
        $IsUser()

    "Bash prompt":
        detect:
            cat "$HOME/.bashrc" | grep -q "PS1=.*\\\W.*"
        apply:
            sed -i "s/\(PS1=.*\)\\\w/\1\\\W/" "$HOME/.bashrc"

"GNOME":
    term:
        $IsRoot()

    "Install Core Applications":
        detect:
            dpkg -s gnome-terminal       2>/dev/null | grep -q "Status: install ok installed"
            dpkg -s gnome-tweaks         2>/dev/null | grep -q "Status: install ok installed"
            dpkg -s gnome-calculator     2>/dev/null | grep -q "Status: install ok installed"
            dpkg -s gnome-disk-utility   2>/dev/null | grep -q "Status: install ok installed"
            dpkg -s gnome-system-monitor 2>/dev/null | grep -q "Status: install ok installed"
            
            dpkg -s nautilus             2>/dev/null | grep -q "Status: install ok installed"  # File Maneger
            dpkg -s baobab               2>/dev/null | grep -q "Status: install ok installed"  # Disk Usage Analyzer
            dpkg -s eog                  2>/dev/null | grep -q "Status: install ok installed"  # Image viewer
            dpkg -s update-manager       2>/dev/null | grep -q "Status: install ok installed"
            dpkg -s gvfs-fuse            2>/dev/null | grep -q "Status: install ok installed"  # Userspace virtual filesystem implementation (Gnome Auto mount smb shares)
            dpkg -s gedit                2>/dev/null | grep -q "Status: install ok installed"  # Text Editor
            dpkg -s file-roller          2>/dev/null | grep -q "Status: install ok installed"  # Archive Manager
            echo Installed

        apply:
            apt-get update
            apt-get install --yes gnome-terminal
            apt-get install --yes gnome-tweaks
            apt-get install --yes gnome-calculator
            apt-get install --yes gnome-disk-utility
            apt-get install --yes gnome-system-monitor
            
            apt-get install --yes nautilus              # File Maneger
            apt-get install --yes baobab                # Disk Usage Analyzer
            apt-get install --yes eog                   # Image Viewer
            apt-get install --yes update-manager
            apt-get install --yes gvfs-fuse             # Userspace virtual filesystem implementation (Gnome Auto mount smb shares)
            apt-get install --yes gedit
            apt-get install --yes file-roller           # Archive Manager

    "Application cleanup":
        detect:
            ! dpkg -s gnome-shell-extension-ubuntu-dock 2>/dev/null | grep -q "Status: install ok installed"
        apply:
            apt-get remove --purge --yes gnome-shell-extension-ubuntu-dock

    "Libre Office":
        latent
        detect:
            #https://packages.debian.org/wheezy/libreoffice
            dpkg -s libreoffice-gnome  2>/dev/null | grep -q "Version"
            dpkg -s libreoffice-writer 2>/dev/null | grep -q "Version"
            dpkg -s libreoffice-calc   2>/dev/null | grep -q "Version"
        apply:
            apt-get install --yes libreoffice-gnome
            apt-get install --yes libreoffice-writer
            apt-get install --yes libreoffice-calc

    "Configure managed network devices":
        term:
            cat /etc/issue | grep -q "Ubuntu" || (echo "Only supported on Ubuntu." && exit 1)
        detect:
            [ -f /etc/NetworkManager/conf.d/10-globally-managed-devices.conf ]
        apply:
            echo [keyfile]              | tee    /etc/NetworkManager/conf.d/10-globally-managed-devices.conf
            echo unmanaged-devices=none | tee -a /etc/NetworkManager/conf.d/10-globally-managed-devices.conf
            
            service network-manager restart

    #https://github.com/PapirusDevelopmentTeam/papirus-icon-theme
    "Shell Icons Pairus-Adapta":
        term:
            cat /etc/issue | grep -q Ubuntu || (echo "Only supported on Ubuntu." && exit 1)
        detect:
            dpkg -s papirus-icon-theme 2>/dev/null | grep -oP "Version: \K.*"
        apply:
            apt-get install --yes software-properties-common # Install add-apt-repository command
            add-apt-repository --yes ppa:papirus/papirus
            apt-get update
            apt-get install --yes papirus-icon-theme

    "Shell Icons Pairus-Adapta":
        term:
            cat /etc/issue | grep -q Debian || (echo "Only supported on Debian." && exit 1)
        detect:
            dpkg -s papirus-icon-theme 2>/dev/null | grep -oP "Version: \K.*"
        apply:
            sh -c "echo 'deb http://ppa.launchpad.net/papirus/papirus/ubuntu bionic main' > /etc/apt/sources.list.d/papirus-ppa.list"
            apt-get install dirmngr
            apt-key adv --recv-keys --keyserver keyserver.ubuntu.com E58A9D36647CAE7F
            apt-get update
            apt-get install --yes papirus-icon-theme

    "Shell Theme Materia-dark":
        detect:
            [ -d /usr/share/themes/Materia ]
        apply:
            # Install XMLLINT and glib-compile-resources
            apt-get install --yes libxml2-utils 
            cat /etc/issue | grep -q Ubuntu && apt-get install --yes libglib2.0-dev-bin

            #wget --no-check-certificate -nv "https://github.com/nana-4/materia-theme/archive/master.tar.gz" -O "$TEMP_PATH/materia-theme.tar.gz"
            #wget --no-check-certificate -nv "https://github.com/nana-4/materia-theme/archive/v20180311.tar.gz" -O "$TEMP_PATH/materia-theme.tar.gz"
            wget --no-check-certificate -nv "https://github.com/nana-4/materia-theme/archive/v20180110.tar.gz" -O "$TEMP_PATH/materia-theme.tar.gz"

            tar -C "$TEMP_PATH" -xzf "$TEMP_PATH/materia-theme.tar.gz"

            #cd "$TEMP_PATH/materia-theme-20180311"
            cd "$TEMP_PATH/materia-theme-20180110"

            ./install.sh


    "Shell Theme Arc":
        latent
        detect:
            dpkg -s arc-theme  2>/dev/null | grep -oP "Version: \K.*"
        apply:
            apt-get install --yes software-properties-common # Install add-apt-repository command
            add-apt-repository ppa:noobslab/themes
            apt-get update
            apt-get install arc-theme

"GNOME":
    term:
        $IsUser()

    "Secondary Click Minimize ":
        detect:
            dconf read /org/gnome/desktop/wm/preferences/action-right-click-titlebar | grep -q minimize
        apply:
            dconf write /org/gnome/desktop/wm/preferences/action-right-click-titlebar "'minimize'"

    "Keyboard Settings":
        detect:
            gsettings get org.gnome.desktop.input-sources sources | grep -q -F "[('xkb', 'us')]"
        apply:
            gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'us')]"

    "Disable Desktop Icons":
        detect:
            dconf read /org/gnome/desktop/background/show-desktop-icons | grep -q "false"
        apply:
            dconf write /org/gnome/desktop/background/show-desktop-icons false

    "File associations":
        detect:
            grep -q "application/octet-stream=code.desktop"  $HOME/.local/share/applications/mimeapps.list
            grep -q "application/x-shellscript=code.desktop" $HOME/.local/share/applications/mimeapps.list
            grep -q "text/markdown=code.desktop"             $HOME/.local/share/applications/mimeapps.list
            grep -q "application/javascript=code.desktop"    $HOME/.local/share/applications/mimeapps.list
        apply:
            file=/usr/share/applications/defaults.list
            file=$HOME/.local/share/applications/mimeapps.list

            grep -q "[Default Applications]"                 $file || echo "[Default Applications]"                 | tee -a $file
            grep -q "application/octet-stream=code.desktop"  $file || echo "application/octet-stream=code.desktop"  | tee -a $file
            grep -q "application/x-shellscript=code.desktop" $file || echo "application/x-shellscript=code.desktop" | tee -a $file
            grep -q "text/markdown=code.desktop"             $file || echo "text/markdown=code.desktop"             | tee -a $file
            grep -q "application/javascript=code.desktop"    $file || echo "application/javascript=code.desktop"    | tee -a $file

    "Terminal Settings":
        detect:
            dconf read /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/scrollbar-policy                | grep -q never
            dconf read /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/use-transparent-background      | grep -q true 
            dconf read /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/background-transparency-percent | grep -q 4
            dconf read /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/default-size-columns            | grep -q 136  
            dconf read /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/default-size-rows               | grep -q 32   
            dconf read /org/gnome/terminal/legacy/default-show-menubar                                                            | grep -q false
        apply:
            dconf write /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/scrollbar-policy                "'never'"
            dconf write /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/use-transparent-background      true
            dconf write /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/background-transparency-percent 4
            dconf write /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/default-size-columns            136
            dconf write /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/default-size-rows               32
            dconf write /org/gnome/terminal/legacy/default-show-menubar                                                            false

    "Nautilus Settings":
        detect:
            dconf read /org/gnome/nautilus/preferences/default-folder-viewer  | grep -q list-view
            dconf read /org/gnome/nautilus/preferences/sort-directories-first | grep -q true
            dconf read /org/gtk/settings/file-chooser/sort-directories-first  | grep -q true
            dconf read /org/gnome/nautilus/list-view/default-zoom-level       | grep -q small
            dconf read /org/gnome/nautilus/list-view/use-tree-view            | grep -q true
        apply:
            dconf write /org/gnome/nautilus/preferences/default-folder-viewer  "'list-view'"
            dconf write /org/gnome/nautilus/preferences/sort-directories-first true
            dconf write /org/gtk/settings/file-chooser/sort-directories-first  true
            dconf write /org/gnome/nautilus/list-view/default-zoom-level       "'small'"
            dconf write /org/gnome/nautilus/list-view/use-tree-view            true

    "Mouse & Touchpad scrolling":
        detect:
            dconf read /org/gnome/desktop/peripherals/touchpad/natural-scroll | grep -q true
            dconf read /org/gnome/desktop/peripherals/mouse/natural-scroll    | grep -q false
        apply:
            dconf write /org/gnome/desktop/peripherals/touchpad/natural-scroll true
            dconf write /org/gnome/desktop/peripherals/mouse/natural-scroll    false

    "Search Providers":
        detect:
            dconf read /org/gnome/desktop/search-providers/disabled | grep -q -F "['org.gnome.Software.desktop', 'org.gnome.Contacts.desktop', 'gnome-calculator.desktop', 'org.gnome.Calendar.desktop', 'org.gnome.Photos.desktop', 'seahorse.desktop']"
        apply:
            dconf write /org/gnome/desktop/search-providers/disabled "['org.gnome.Software.desktop', 'org.gnome.Contacts.desktop', 'gnome-calculator.desktop', 'org.gnome.Calendar.desktop', 'org.gnome.Photos.desktop', 'seahorse.desktop']"

    # https://www.ubuntuupdates.org/package/webupd8/yakkety/main/base/gnome-appfolders-manager
    "Office folders":
        latent
        detect:
            dconf read /org/gnome/desktop/app-folders/folders/Utilities/apps | grep -F "ca.desrt.dconf-editor.desktop"
            dconf read /org/gnome/desktop/app-folders/folders/Office/apps    | grep -F ""
        apply:
            apps=$(dconf read /org/gnome/desktop/app-folders/folders/Sundry/apps)
            apps=${apps/", 'ca.desrt.dconf-editor.desktop'"/}
            dconf write /org/gnome/desktop/app-folders/folders/Sundry/apps "$apps" 

            dconf write /org/gnome/desktop/app-folders/folders/Office/name "'Office'"
    
            children=$(dconf read /org/gnome/desktop/app-folders/folder-children)
            children=${children/", 'Office'"/}
            children=${children/]/", 'Office']"}
            dconf write /org/gnome/desktop/app-folders/folder-children "$children" 

            apps="['libreoffice-writer.desktop', 'libreoffice-draw.desktop', 'libreoffice-impress.desktop', 'libreoffice-startcenter.desktop', 'libreoffice-calc.desktop', 'libreoffice-math.desktop']"
            dconf write /org/gnome/desktop/app-folders/folders/Office/apps "$apps"

    "Utilities Folders":
        detect:
            dconf read /org/gnome/desktop/app-folders/folder-children        | grep -q -F "Utilities"
            dconf read /org/gnome/desktop/app-folders/folders/Utilities/apps | grep -q -F "['org.gnome.baobab.desktop', 'eog.desktop', 'org.gnome.FileRoller.desktop', 'org.gnome.DiskUtility.desktop', 'org.gnome.Terminal.desktop', 'gnome-system-monitor.desktop', 'yelp.desktop', 'org.gnome.tweaks.desktop', 'gnome-control-center.desktop', 'gnome-session-properties.desktop', 'update-manager.desktop', 'im-config.desktop', 'gnome-language-selector.desktop', 'software-properties-gtk.desktop']"
        apply:
            dconf write /org/gnome/desktop/app-folders/folder-children "['Utilities']"
            dconf write /org/gnome/desktop/app-folders/folders/Utilities/apps "['org.gnome.baobab.desktop', 'eog.desktop', 'org.gnome.FileRoller.desktop', 'org.gnome.DiskUtility.desktop', 'org.gnome.Terminal.desktop', 'gnome-system-monitor.desktop', 'yelp.desktop', 'org.gnome.tweaks.desktop', 'gnome-control-center.desktop', 'gnome-session-properties.desktop', 'update-manager.desktop', 'im-config.desktop', 'gnome-language-selector.desktop', 'software-properties-gtk.desktop']"

    "Favorite apps":
        detect:
            dconf read /org/gnome/shell/favorite-apps | grep -q -F "['org.gnome.Nautilus.desktop', 'org.gnome.Terminal.desktop', 'brave.desktop', 'code.desktop']"
        apply:
            dconf write /org/gnome/shell/favorite-apps "['org.gnome.Nautilus.desktop', 'org.gnome.Terminal.desktop', 'brave.desktop', 'code.desktop']"
    
    "Disable Activities Overview Hot Corner":
        detect:
            dconf read /org/gnome/shell/enable-hot-corners | grep -q -F "false"
        apply:
            dconf write /org/gnome/shell/enable-hot-corners "false"

    "User Themes Extension":
        # https://extensions.gnome.org/extension-info/?pk=19&shell_version=3.30
        # https://extensions.gnome.org/extension/19/user-themes/
        detect:
            [ -d "$HOME/.local/share/gnome-shell/extensions/user-theme@gnome-shell-extensions.gcampax.github.com" ]
            
            dconf read /org/gnome/shell/enabled-extensions | grep -q -F "['user-theme@gnome-shell-extensions.gcampax.github.com']"
        apply:
            wget -nv $USER_THEMES_EXTENSION -O "$TEMP_PATH/extension.zip"
            
            mkdir -p "$HOME/.local/share/gnome-shell/extensions/user-theme@gnome-shell-extensions.gcampax.github.com"
            
            unzip -o "$TEMP_PATH/extension.zip" -d "$HOME/.local/share/gnome-shell/extensions/user-theme@gnome-shell-extensions.gcampax.github.com"

            dconf write /org/gnome/shell/enabled-extensions "['user-theme@gnome-shell-extensions.gcampax.github.com']"

    "Shell Theme Materia-dark":
        detect:
            dconf read /org/gnome/shell/extensions/user-theme/name   | grep -oP "(?<=')Materia-dark(?=')"
            dconf read /org/gnome/desktop/interface/gtk-theme        | grep -oP "(?<=')Materia-dark(?=')"
        apply:
            dconf write /org/gnome/shell/extensions/user-theme/name "'Materia-dark'"
            dconf write /org/gnome/desktop/interface/gtk-theme      "'Materia-dark'"

    "Shell Icons Pairus-Adapta":
        detect:
            dconf read /org/gnome/desktop/interface/icon-theme | grep -oP "(?<=')Papirus-Dark(?=')"
        apply:
            dconf write /org/gnome/desktop/interface/icon-theme "'Papirus-Dark'"

    "Background":
        detect:
            dconf read /org/gnome/desktop/background/picture-uri  | grep -q "gnome-background-4k.png"
            dconf read /org/gnome/desktop/screensaver/picture-uri | grep -q "logon-background-4k.png"
        apply:
            cp $SCRIPT_ROOT/resources/gnome-background-4k.png  "$HOME/Pictures/gnome-background-4k.png"
            cp $SCRIPT_ROOT/resources/logon-background-4k.png  "$HOME/Pictures/logon-background-4k.png"

            dconf write /org/gnome/desktop/background/picture-uri  "'file://$HOME/Pictures/gnome-background-4k.png'"
            dconf write /org/gnome/desktop/screensaver/picture-uri "'file://$HOME/Pictures/logon-background-4k.png'"
            

    "New Document":
        detect:
            [ -f "$HOME/Templates/README.md" ]
        apply:
            echo > "$HOME/Templates/README.md"

    "Top Bar Clock Settings":
        detect:
            dconf read /org/gnome/desktop/interface/clock-show-date | grep -q true
            dconf read /org/gnome/shell/calendar/show-weekdate      | grep -q true
        apply:
            dconf write /org/gnome/desktop/interface/clock-show-date true
            dconf write /org/gnome/shell/calendar/show-weekdate      true

    "Disable screen dim when inactive":
        detect:
            dconf read /org/gnome/settings-daemon/plugins/power/idle-dim | grep -q -F false
        apply:
            dconf write /org/gnome/settings-daemon/plugins/power/idle-dim false
    
    "Never blank screen":
        detect:
            dconf read /org/gnome/desktop/session/idle-delay | grep -q -F "uint32 0"
        apply:
            dconf write /org/gnome/desktop/session/idle-delay "uint32 0"

"XAPP":
    term:
        $IsRoot()

    "Cloud Station Drive":
        detect:
            dpkg -s synology-cloud-station 2>/dev/null | grep -oP "Version: \K.*"
        apply:
            wget -nv $CLOUD_STATION_DRIVE -O "$TEMP_PATH/synology-cloud-station-drive.x86_64.deb"
            
            apt-get --yes install "$TEMP_PATH/synology-cloud-station-drive.x86_64.deb"
    
    "Libre LanguageTool":
        latent
        detect:
            dpkg -s libreoffice-java-common 2>/dev/null | grep -oP "Version: \K.*"
            unopkg list | grep -F "Identifier: org.openoffice.languagetool.oxt"
            [ -f ~/bin/LanguageTool-4.3.oxt ]
        apply:
            apt-get install --yes libreoffice-java-common
            wget -nv https://languagetool.org/download/LanguageTool-4.3.oxt -O ~/bin/LanguageTool-4.3.oxt
            unopkg add $HOME/bin/LanguageTool-4.3.oxt --force
    
    "Google chrome":
        detect:
            dpkg -s google-chrome-stable 2>/dev/null | grep -oP "Status: install ok installed"
            dpkg -s google-chrome-stable 2>/dev/null | grep -oP "Version: \K.*"
        apply:
            wget -nv https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -O "$TEMP_PATH/google-chrome-stable_current_amd64.deb"
            #sudo -n apt-get install --yes libappindicator1 libindicator7 libpango1.0-0 libpangox-1.0-0
            apt-get install --yes "$TEMP_PATH/google-chrome-stable_current_amd64.deb"

    "Firefox":
        term:
            cat /etc/issue | grep -q Ubuntu || (echo "Only supported on Ubuntu." && exit 1)
        detect:
            dpkg -s firefox 2>/dev/null | grep -oP "Version: \K.*"
        apply:
            apt-get install --yes firefox
            

    "Brave":
        term:
            cat /etc/issue | grep -q Ubuntu || (echo "Only supported on Ubuntu." && exit 1)
        detect:
            dpkg -s Brave 2>/dev/null | grep -oP "Version: \K.*"
        apply:
            wget -qO- https://s3-us-west-2.amazonaws.com/brave-apt/keys.asc | apt-key add -

            echo "deb [arch=amd64] https://s3-us-west-2.amazonaws.com/brave-apt `lsb_release -sc` main" | tee -a /etc/apt/sources.list.d/brave-`lsb_release -sc`.list

            apt-get update
            apt-get install --yes brave

    "vscode":
        detect:
            dpkg -s code 2>/dev/null | grep -oP "Version: \K.*"
        apply:
            wget -nv -L https://go.microsoft.com/fwlink/?LinkID=760868 -O "$TEMP_PATH/vscode.deb"
            apt-get install --yes "$TEMP_PATH/vscode.deb"

"XAPP":
    term:
        $IsUser()

    "vscode customizations":
        detect:
            code  --list-extensions | grep ms-vscode.csharp
            code  --list-extensions | grep msjsdiag.debugger-for-chrome
            code  --list-extensions | grep zhengxiaoyao0716.intelligence-change-case
            code  --list-extensions | grep rprouse.theme-obsidian
            code  --list-extensions | grep ms-vscode.powershell
            code  --list-extensions | grep ms-vscode.Go
            code  --list-extensions | grep sibiraj-s.vscode-scss-formatter
        apply:
            code --install-extension ms-vscode.csharp
            code --install-extension msjsdiag.debugger-for-chrome
            code --install-extension zhengxiaoyao0716.intelligence-change-case
            code --install-extension rprouse.theme-obsidian
            code --install-extension ms-vscode.PowerShell
            code --install-extension ms-vscode.Go
            code --install-extension sibiraj-s.vscode-scss-formatter

            mkdir -p "$HOME/.config/Code/User"
            cp "$SCRIPT_ROOT/resources/vscode.keybindings.json" "$HOME/.config/Code/User/keybindings.json"
            cp "$SCRIPT_ROOT/resources/vscode.settings.json"    "$HOME/.config/Code/User/settings.json"

"KERNEL":
    term:
        $IsRoot()

    "Kernel Cleanup":
        latent
        detect:
            set +e
            dpkg --list | grep -P "linux-image-(extra-)?(\d+\.\d+\.\d+-\d+)" | awk '{ print $2 }' | grep -v "`uname -r`"
            ! [ $? = 0 ] 
        apply:
            dpkg --list | grep -P "linux-image-(extra-)?(\d+\.\d+\.\d+-\d+)" | awk '{ print $2 }' | grep -v "`uname -r`" | xargs apt-get --yes purge

"SYSTEM":
    term:
        $IsRoot()

    "Extended File Allocation Table (exFAT) support":
        latent
        detect:
            dpkg -s exfat-fuse exfat-utils
        apply:
            apt-get install --yes exfat-fuse exfat-utils
    
    "Canonical Livepatch Service":
        latent
        detect:
            snap list canonical-livepatch
        apply:
            snap install canonical-livepatch
            canonical-livepatch enable 02fe4cc1f3644136a060df0e68dcd1a7

    "Remove unused dependencies":
        detect:
            set +e
            
            apt-get -s autoremove | grep Remv
            ! [ $? = 0 ]
        apply:
            apt-get autoremove --yes

    "Fix broken dependencies":
        detect:
            apt-get check > /dev/null
        apply:
            apt-get clean --yes
            apt-get autoclean --yes
            apt-get install --yes -f

    "Distribution upgrade":
        detect:
            set +e

            apt-get update
            apt-get -s dist-upgrade | grep Inst
            ! [ $? = 0 ]
        apply:
            apt-get --yes dist-upgrade
