# Proton

Proton is a standalone multi platform configuration tool. The configuration is hierarchical and idempotent by design. It's goal is to make system configuration as easy and flexible as possible. No servers or infrastructure is needed, just one command and a config file. The target audience: developers who needs some basic system setup and IT professionals for configuring their infrastructure environment. Proton allows you to use any scripting language available on your computer and it is written in Go so it can be made available for any platform.

### Downloads

|               |                                                  Proton 2.0.0-rc.5                                                  |
| :------------ | :-----------------------------------------------------------------------------------------------------------------: |
| Linux (x64)   | [.tar.gz download](https://gitlab.com/heerschop/proton/raw/2.0.0-rc.5/binaries/proton-2.0.0-rc.5-linux-x64.tar.gz)  |
| macOS (x64)   | [.tar.gz download](https://gitlab.com/heerschop/proton/raw/2.0.0-rc.5/binaries/proton-2.0.0-rc.5-darwin-x64.tar.gz) |
| Windows (x64) |   [.zip download](https://gitlab.com/heerschop/proton/raw/2.0.0-rc.5/binaries/proton-2.0.0-rc.5-windows-x64.zip)    |

[Previous Releases](./doc/previous-releases.md)

### Usage

```
./proton --help          # Help and examples.
./proton --init simple   # Show starter template.
./proton --init complete # Show complete template.
```

### Basic commands

Create new starter configuration

```
./proton --init simple > template-simple.sh
```

Proton detect configuration state

```
./proton template-simple.sh
./proton template-simple.sh --filter ".*"
```

Proton apply the configuration

```
./proton template-simple.sh --apply
./proton template-simple.sh --apply --latent
```

### Documentation

- [Shebang](doc/technical-documentation.md#Shebang)
- [Shell Selection](doc/technical-documentation.md#Shell-Selection)
- [Debug configuration the (Show argument)](<doc/technical-documentation.md#Debug-configuration-the-(Show-argument)>)
- [Configuration keywords](doc/technical-documentation.md#Configuration-keywords)
- [Configuration structure](doc/technical-documentation.md#Configuration-structure)

### Example config files

- [Template simple Linux / MacOS](examples/template-simple.sh)
- [Template simple Windows](examples/template-simple-win.sh)
- [Template complete](examples/template-complete.sh)
- [Multiple scripts](examples/multi-scripts.sh)
- [Multiple scripts Windows](examples/multi-scripts-win.sh)
- [Golang binary](examples/golang-binary.sh)
- [Golang development](examples/golang-dev.sh)
- [Advanced example](examples/advanced-config.sh)

### Simple config template

You can have multiple **Category items** and **Configuration items**

```bash
#!proton

# This simple example demonstrates the basic functionality of
# proton by creating a single text file.

# To create this template
# run: proton --init simple > template-simple.sh

# To show the resulting actions scripts
# run: proton --show actions

settings:
    SHELL: "/bin/bash"

templates:
    init:
        set -e

"My Category":
    "My Configuration":
        detect:
            echo Script to detect the file.
            [ -f proton-example ]

        apply:
            echo Script to create the file.
            echo "hello proton" >  proton-example

        remove:
            echo Script to remove the file.
            rm proton-example
```

## Development

### Tools

- [Go compiler](https://golang.org/) - Go is an open source programming language that makes it easy to build simple, reliable, and efficient software.
- [Visual Studio Code](https://code.visualstudio.com/) - Visual Studio Code is a lightweight but powerful source code editor.
- [Go for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=ms-vscode.Go) - This extension adds rich language support for the Go language to VS Code.

### Basic Build Steps

```
git clone git@gitlab.com:heerschop/proton.git
go build
cd binaries
./build.sh --apply
```

### Debug analyzer

```
./proton template-simple.sh --verbose analyze
```
