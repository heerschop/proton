// +build windows

package main

import (
	"os"
	"syscall"
	"unsafe"
)

var (
	kernel32                       = syscall.NewLazyDLL("kernel32.dll")
	procGetConsoleScreenBufferInfo = kernel32.NewProc("GetConsoleScreenBufferInfo")
	setConsoleMode                 = kernel32.NewProc("SetConsoleMode")
	getConsoleMode                 = kernel32.NewProc("GetConsoleMode")
)

type short int16
type word uint16
type dword uint32

type coord struct {
	x short
	y short
}

type smallRect struct {
	left   short
	top    short
	right  short
	bottom short
}

type consoleScreenBufferInfo struct {
	size              coord
	cursorPosition    coord
	attributes        word
	window            smallRect
	maximumWindowSize coord
}

func test2() (int, int) {
	handle := syscall.Handle(os.Stdout.Fd())
	var csbi consoleScreenBufferInfo
	procGetConsoleScreenBufferInfo.Call(uintptr(handle), uintptr(unsafe.Pointer(&csbi)))

	var x int
	var y int
	x = int(csbi.size.x)
	y = int(csbi.size.y)

	return x, y

}

const ENABLE_VIRTUAL_TERMINAL_PROCESSING = 0x0004

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func getWinsize() (*winsize, error) {
	handle := syscall.Handle(os.Stdout.Fd())

	var info consoleScreenBufferInfo
	procGetConsoleScreenBufferInfo.Call(uintptr(handle), uintptr(unsafe.Pointer(&info)))

	ws := new(winsize)
	ws.Col = uint16(info.size.x)
	ws.Row = uint16(info.size.y)

	return ws, nil
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func enableAnsiSupport() {
	handle := syscall.Handle(os.Stdout.Fd())
	var mode dword

	getConsoleMode.Call(uintptr(handle), uintptr(unsafe.Pointer(&mode)))
	mode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING
	setConsoleMode.Call(uintptr(handle), uintptr(mode))
}
