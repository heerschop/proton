package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

type winsize struct {
	Row    uint16
	Col    uint16
	Xpixel uint16
	Ypixel uint16
}

type spinner struct {
	wgRunning sync.WaitGroup
	wgStopped sync.WaitGroup
}

type presenter struct {
	spinner  *spinner
	width    int
	category string
	name     string
}
type presenterWriter struct {
	writer   io.Writer
	content  *int
	previous *string
	category string
	name     string
}

type actionText struct {
	text  string
	color string
	count int
}

type actionIntend int

const (
	actionDetect actionIntend = iota + 1
	actionApply
	actionForce
)

type actionResult int

const (
	resultDesired actionResult = iota + 1
	resultMissing
	resultDestroy
	resultUpdated
	resultRemoved
	resultSkipped
	resultLatent
	resultFailed
	resultComponent
	resultFiltered
)

var errEmptyScript = errors.New("Empty script")

type streamPostfix struct {
	writer  io.Writer
	text    string
	content bool
}
type logWriter struct {
	writer io.Writer
	prefix string
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (logWriter *logWriter) Write(p []byte) (int, error) {
	text := string(p)

	current := time.Now().Format("2006-01-02 15:04:05.000")

	text = strings.TrimRight(text, "\n")
	buffer := ""

	for _, line := range strings.Split(text, "\n") {
		buffer += fmt.Sprintf("%s [%s] : %s\n", current, logWriter.prefix, line)
	}

	return fmt.Fprint(logWriter.writer, buffer)
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (presenterWriter *presenterWriter) Write(p []byte) (int, error) {

	if *presenterWriter.content == 0 {
		*presenterWriter.previous = string(p)
	}

	if *presenterWriter.content == 1 {
		logWidth := 180 - 32 // ( 32 = log prefix -> 2018-12-18 06:50:08.410 [OUT] : )
		seperator := strings.Repeat("-", logWidth)
		fmt.Fprintln(presenterWriter.writer)
		fmt.Fprintf(presenterWriter.writer, "%s : %s\n", presenterWriter.category, presenterWriter.name)
		fmt.Fprintf(presenterWriter.writer, "%s\n", seperator)
		fmt.Fprintln(presenterWriter.writer, *presenterWriter.previous)
	}

	*presenterWriter.content++

	if *presenterWriter.content > 1 && p != nil {
		return presenterWriter.writer.Write(p)
	}

	return 0, nil
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (presenterWriter *presenterWriter) Completed(result string, elapsedTime float32) {
	logWidth := 180 - 32 // ( 32 = log prefix -> 2018-12-18 06:50:08.410 [OUT] : )
	if *presenterWriter.content > 1 {
		seperator := strings.Repeat("-", logWidth)
		fmt.Fprintf(presenterWriter.writer, "%s\n", seperator)
		fmt.Fprintf(presenterWriter.writer, "%-7s %*.3fs\n", result, logWidth-9, elapsedTime)
		fmt.Fprintln(presenterWriter.writer)
	} else {
		previous := strings.Trim(*presenterWriter.previous, "\n")
		message := fmt.Sprintf("%s  %-7s %8.3fs\n", previous, result, elapsedTime)
		name := presenterWriter.category + " : " + presenterWriter.name

		logWidth -= len(name)

		if logWidth < 0 {
			logWidth = 0
		}

		if len(message) > logWidth {
			presenterWriter.Write(nil)
			presenterWriter.Completed(result, elapsedTime)

		} else {
			fmt.Fprintf(presenterWriter.writer, "%s %*s\n", name, logWidth, message)
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (presenter presenter) start() {
	fmt.Printf("%s : %s", ansiBold+presenter.category+ansiNormal, presenter.name)

	if presenter.spinner != nil {
		outline := presenter.width - len(presenter.category+" : "+presenter.name) - 6
		fmt.Printf(strings.Repeat(" ", outline))
		presenter.spinner.start()
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (presenter presenter) stop(color string, message string, result string) {
	outline := presenter.width - len(presenter.category+" : "+presenter.name) - 12
	if presenter.spinner != nil {
		presenter.spinner.stop()
		fmt.Printf(strings.Repeat("\b", outline+6))
	}

	if len(message) > outline {
		crop := outline - 14
		if crop < 0 {
			crop = 0
		}

		message = message[:crop] + "...+" + strconv.Itoa(len(message)-crop) + " more"
	}

	fmt.Printf("  %s%*s%s  [%s%7s%s]\n", ansiDim+ansiItalic, outline, message, ansiNormal, color, result, ansiNormal)
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func executeScript(description string, nameLength int, showSpinner bool, writer io.Writer, handler func(presenter presenter, outLogger io.Writer, errLogger io.Writer) (string, bool)) {
	expression := regexp.MustCompile(`([^.]*)(\.(.*))?`)
	matches := expression.FindStringSubmatch(description)

	presenter := presenter{
		spinner:  nil,
		width:    80,
		category: fmt.Sprintf("%-*s", nameLength, matches[1]),
		name:     matches[3],
	}
	outLogger := &logWriter{
		writer: writer,
		prefix: "OUT",
	}
	errLogger := &logWriter{
		writer: writer,
		prefix: "ERR",
	}

	if showSpinner {
		presenter.spinner = &spinner{}
	}

	if size, err := getWinsize(); err == nil {
		presenter.width = int(size.Col) - 2
	}

	start := time.Now()

	content := 0
	previous := ""

	outPresenter := presenterWriter{
		writer:   outLogger,
		category: matches[1],
		name:     matches[3],
		content:  &content,
		previous: &previous,
	}
	errPresenter := presenterWriter{
		writer:   errLogger,
		category: matches[1],
		name:     matches[3],
		content:  &content,
		previous: &previous,
	}

	result, success := handler(presenter, &outPresenter, &errPresenter)

	elapsed := time.Since(start)

	if success {
		outPresenter.Completed(result, (float32(elapsed.Nanoseconds()) / float32(time.Second)))
	} else {
		errPresenter.Completed(result, (float32(elapsed.Nanoseconds()) / float32(time.Second)))
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func actionProcessor(totalStartTime time.Time, log io.Writer, actions []actionConfig, init actionScript, final actionScript, filter string, scriptRoot string, globalEnvironment map[string]map[string]configContent, remove bool, intend actionIntend, showSpinner bool, applyLatent bool, showSkipped bool, showLatent bool, nameLength int) {

	expression := regexp.MustCompile("(?i)" + filter)

	actionText := map[actionResult]*actionText{
		resultDesired: {
			text:  "Desired",
			color: ansiGreen,
			count: 0,
		},
		resultMissing: {
			text:  "Missing",
			color: ansiOrange,
			count: 0,
		},
		resultDestroy: {
			text:  "Destroy",
			color: ansiOrange,
			count: 0,
		},
		resultUpdated: {
			text:  "Updated",
			color: ansiGreen,
			count: 0,
		},
		resultRemoved: {
			text:  "Removed",
			color: ansiGreen,
			count: 0,
		},
		resultSkipped: {
			text:  "Skipped",
			color: ansiYellow,
			count: 0,
		},
		resultLatent: {
			text:  "Latent",
			color: ansiYellow,
			count: 0,
		},
		resultFailed: {
			text:  "Failed",
			color: ansiRed,
			count: 0,
		},
		resultComponent: {
			text:  "Component",
			color: ansiNormal,
			count: 0,
		},
		resultFiltered: {
			text:  "Filtered",
			color: ansiYellow,
			count: 0,
		},
	}

	tempDir := newTempDir()

	removeFiles(tempDir + "*")

	outLogger := &logWriter{
		writer: log,
		prefix: "OUT",
	}

	if len(init.script) > 0 {
		executeScript("Global.Init", nameLength, showSpinner, log, func(presenter presenter, outLogger io.Writer, errLogger io.Writer) (string, bool) {
			presenter.start()
			if message, error := init.execute(tempDir, outLogger, errLogger, scriptRoot, globalEnvironment["init"]); error == nil {
				presenter.stop(ansiGreen, message, "Success")
				return "Success", true
			} else {
				presenter.stop(ansiRed, message, "Error")
				return "Error", false
			}
		})
	}

	actionCount := 0

	for _, action := range actions {
		if action.component.value {
			actionText[resultComponent].count++
			continue
		}

		if !expression.MatchString(action.name) {
			actionText[resultFiltered].count++
			continue
		}

		result := resultLatent
		skipped := false

		executeScript(action.name, nameLength, showSpinner, log, func(presenter presenter, outLogger io.Writer, errLogger io.Writer) (string, bool) {
			message := ""
			success := false

			if action.error != "" {
				result = resultFailed
				fmt.Fprintln(errLogger, action.error)
			}

			if message, success = action.evaluateTerms(tempDir, outLogger, errLogger, scriptRoot, action.environment["term"]); !success {
				result = resultSkipped
				skipped = true
			}

			if (!action.latent.value || applyLatent || showLatent) && (!skipped || showSkipped) {
				presenter.start()
			}

			if (!action.latent.value || applyLatent) && !skipped && result != resultFailed {
				message, result = action.execute(tempDir, outLogger, errLogger, scriptRoot, remove, intend)
			}

			item := actionText[result]
			item.count++

			if (!action.latent.value || applyLatent || showLatent) && (!skipped || showSkipped) {
				actionCount++
				presenter.stop(item.color, message, item.text)
			}

			return item.text, result != resultFailed
		})
	}

	if len(final.script) > 0 {
		executeScript("Global.Final", nameLength, showSpinner, log, func(presenter presenter, outLogger io.Writer, errLogger io.Writer) (string, bool) {
			presenter.start()
			if message, error := final.execute(tempDir, outLogger, errLogger, scriptRoot, globalEnvironment["final"]); error == nil {
				presenter.stop(ansiGreen, message, "Success")
				return "Success", true
			} else {
				presenter.stop(ansiRed, message, "Error")
				return "Error", false
			}
		})
	}

	elapsed := time.Since(totalStartTime)

	fmt.Fprintf(outLogger, "Completed in: %s\n", elapsed)

	if actionCount == 0 {
		fmt.Println()
		printNothingToDoMessage("Nothing to do, no actions available or selected.", logFileName)
	}

	fmt.Println()

	fmt.Printf(ansiBold+"Completed in: "+ansiNormal+ansiDim+"%02.0f:%02.0f:%06.03f"+ansiNormal+"\n", elapsed.Hours(), elapsed.Minutes(), elapsed.Seconds())
	fmt.Println()
	printFooter(actionText)
	fmt.Println()

	os.RemoveAll(tempDir)
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func printNothingToDoMessage(message string, fileName string) {
	fmt.Println(message + " Use one of the folowing arguments to select more actions:")
	fmt.Println(ansiBold + "--filter \".*\" :" + ansiNormal + ansiDim + " Select all actions." + ansiNormal)
	fmt.Println(ansiBold + "--latent      :" + ansiNormal + ansiDim + " Select latent actions." + ansiNormal)
	fmt.Println(ansiBold + "--skipped     :" + ansiNormal + ansiDim + " Select skipped actions." + ansiNormal)
	fmt.Println()
	fmt.Printf("Inspect the '%s' log file for more information.\n", fileName)
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (action actionConfig) evaluateTerms(tempDir string, outLogger io.Writer, errLogger io.Writer, scriptRoot string, environment map[string]configContent) (string, bool) {
	lastMessage := ""

	for _, script := range action.terms {
		// fmt.Fprintf(outLogger, "Executing: %s (line %d)\n", script.name, script.offset)
		message, error := script.execute(tempDir, outLogger, errLogger, scriptRoot, environment)
		if message != "" {
			lastMessage = message
		}

		if error != nil {
			return lastMessage, false
		}
	}

	return lastMessage, true
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (action actionConfig) execute(tempDir string, outLogger io.Writer, errLogger io.Writer, scriptRoot string, remove bool, intend actionIntend) (string, actionResult) {
	message, error := action.detect.execute(tempDir, outLogger, errLogger, scriptRoot, action.environment["detect"])

	if remove {
		if intend != actionForce {

			switch error.(type) {
			default:
				if error != nil {
					return message, resultDesired
				}
				if intend != actionApply {
					return message, resultDestroy
				}
			case *environmentError:
				return message, resultFailed
			}
		}

		if message, error := action.remove.execute(tempDir, outLogger, errLogger, scriptRoot, action.environment["remove"]); error == nil {
			return message, resultRemoved
		}

	} else {
		if intend != actionForce {

			switch error.(type) {
			default:
				if error == nil {
					return message, resultDesired
				}
				if intend != actionApply {
					return message, resultMissing
				}
			case *environmentError:
				return message, resultFailed
			}
		}

		if message, error := action.apply.execute(tempDir, outLogger, errLogger, scriptRoot, action.environment["apply"]); error == nil {
			return message, resultUpdated
		}
	}

	return message, resultFailed
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (spinner *spinner) start() {
	chars := "-\\|/"
	count := len(chars)
	index := 0

	spinner.wgRunning.Add(1)
	spinner.wgStopped.Add(1)

	go func() {
		fmt.Printf("     ")
		for {
			fmt.Printf("\b\b\b\b\b[ %c ]", chars[index%count])

			if !waitTimeout(&spinner.wgRunning, time.Millisecond*150) {
				break
			}

			index++
		}

		fmt.Printf("\b\b\b\b\b")

		spinner.wgStopped.Done()
	}()
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (spinner *spinner) stop() {
	spinner.wgRunning.Done()
	spinner.wgStopped.Wait()
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func waitTimeout(wg *sync.WaitGroup, timeout time.Duration) bool {
	c := make(chan struct{})
	go func() {
		defer close(c)
		wg.Wait()
	}()
	select {
	case <-c:
		return false // completed normally
	case <-time.After(timeout):
		return true // timed out
	}
}

type environmentError struct {
	message string
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (error *environmentError) Error() string {
	return error.message
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (action actionScript) evaluateExpression(outLogger io.Writer, errLogger io.Writer) (bool, string, error) {
	if len(action.script) == 0 {
		return false, "Empty script", nil
	}

	if action.script[0] == "#!proton-expression" {
		fmt.Fprintf(outLogger, "Evaluating expression: %s (line %d)\n", action.name, action.offset)

		if len(action.script) <= 1 {
			error := errors.New("No proton expressions specified")
			fmt.Fprintf(errLogger, "%s\n", error.Error())
			return false, error.Error(), error
		}

		expression := strings.Join(action.script[1:], " ")

		expression = os.ExpandEnv(expression)

		result, error := evaluateExpression(expression)

		if !result && error == nil {
			error = errors.New("false")
		}

		message := ""

		if error != nil {
			error = errors.New(expression + " => " + error.Error())
			fmt.Fprintln(errLogger, error.Error())
			message = error.Error()
		} else {
			message = fmt.Sprintf("%s => true", expression)
			fmt.Fprintln(outLogger, message)
		}

		return true, message, error
	}

	return false, "", nil
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (action actionScript) execute(tempDir string, outLogger io.Writer, errLogger io.Writer, scriptRoot string, environment map[string]configContent) (string, error) {
	environmentErrorCount := 0

	for name, value := range environment {
		if value.Type == configError && value.Inherited == 0 {
			fmt.Fprintf(errLogger, "Failed to evaluate environment variable '%s': %s.\n", name, value.Content[0])
			environmentErrorCount++
		}
	}

	if environmentErrorCount != 0 {
		message := fmt.Sprintf("Failed to evaluate %d environment variable", environmentErrorCount)

		if environmentErrorCount == 0 || environmentErrorCount > 1 {
			message += "s"
		}

		return message, &environmentError{message}
	}

	if evaluated, message, error := action.evaluateExpression(outLogger, errLogger); evaluated {
		return message, error
	}

	fileName, arguments, shell, shellArgs, error := generateScript(action.script, action.offset, tempDir+"action-script")

	if error != nil {
		fmt.Fprintf(errLogger, "%s\n", error)

		return error.Error(), error
	}

	cmd := exec.Command(fileName, arguments...)

	outReader, error := cmd.StdoutPipe()

	if error != nil {
		fmt.Fprintf(errLogger, "%s\n", error)
		return error.Error(), error
	}

	errReader, error := cmd.StderrPipe()

	if error != nil {
		fmt.Fprintf(errLogger, "%s\n", error)
		return error.Error(), error
	}

	cmd.Env = os.Environ()

	for key, value := range environment {
		cmd.Env = append(cmd.Env, key+"="+strings.Join(value.Content, "\n"))
	}

	cmd.Env = append(cmd.Env, "SHELL="+shell)
	cmd.Env = append(cmd.Env, "SHELL_ARGS="+shellArgs)

	error = cmd.Start()

	if error != nil {
		fmt.Fprintf(errLogger, "%s\n", error)
		return error.Error(), error
	}

	var wg sync.WaitGroup

	wg.Add(2)

	line := ""

	lastErrLine := lastLine{
		writer: errLogger,
		line:   &line,
	}
	lastOutLine := lastLine{
		writer: outLogger,
		line:   &line,
	}

	go logger(&wg, errReader, &lastErrLine)
	go logger(&wg, outReader, &lastOutLine)

	wg.Wait()

	error = cmd.Wait()

	return line, error
}

type lastLine struct {
	writer io.Writer
	line   *string
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (lastLine *lastLine) Write(p []byte) (int, error) {
	*lastLine.line = strings.Trim(string(p), "\n")
	return lastLine.writer.Write(p)
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func logger(wg *sync.WaitGroup, reader io.Reader, logWriter io.Writer) {
	scanner := bufio.NewScanner(reader)

	for scanner.Scan() {
		fmt.Fprintln(logWriter, scanner.Text())
	}

	wg.Done()
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func removeFiles(path string) error {
	var err error

	files, err := filepath.Glob(path)

	if err != nil {
		return err
	}

	for _, file := range files {
		os.Remove(file)
	}

	return nil
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func newTempDir() string {
	tempDir := os.TempDir()

	tempDir = strings.TrimRight(tempDir, string(os.PathSeparator)) + string(os.PathSeparator) + getUniqueName()

	os.Mkdir(tempDir, os.ModePerm)

	return tempDir + string(os.PathSeparator)

}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func getUniqueName() string {
	value := time.Now().UnixNano() / (1000 * 100)

	return "proton-" + strconv.FormatInt(value, 32)
	//return "proton-dh2v2n7om"
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func getShell(shebang string) (string, string) {
	expression := regexp.MustCompile(`^#!((\".*?\")|(.*?))\s*(\s.*)?$`)
	matches := expression.FindStringSubmatch(shebang)

	if len(matches) == 5 {
		return strings.Trim(matches[1], "\""), strings.TrimSpace(matches[4])
	}

	return strings.TrimLeft(shebang, "#!"), ""
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func getScriptInterpreter(script []string, fileName string) (string, []string, string, string, string, bool) {
	shell, shellArgs := getShell(script[0])

	expression := regexp.MustCompile(`^(#!(.*?))?(\s|#)!(\.\w+)$`)

	matches := expression.FindStringSubmatch(script[0])

	if len(matches) == 5 {
		scriptFile := fileName + matches[4]

		expression := regexp.MustCompile(`^((\".*?\")|(.*?))\s*(\s(.*))?$`)

		matches := expression.FindStringSubmatch(matches[2])

		fileName = matches[1]
		arguments := stringSplit(matches[5], " ")

		if fileName != "" {
			shell = strings.Trim(fileName, "\"")
			shellArgs = strings.TrimSpace(matches[5])
			arguments = append(arguments, scriptFile)
		} else {
			fileName = scriptFile
			shell = scriptFile
		}

		fileName = strings.Trim(fileName, "\"")

		return fileName, arguments, scriptFile, shell, shellArgs, true
	}

	return fileName, []string{}, fileName, shell, shellArgs, false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func stringSplit(s string, sep string) []string {
	var result []string

	for _, item := range strings.Split(s, sep) {
		if item != "" {
			result = append(result, item)
		}
	}

	return result
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func generateScript(script []string, offset int, fileName string) (string, []string, string, string, error) {

	if len(script) == 0 {
		return "", nil, "", "", errEmptyScript
	}

	fileName, arguments, scriptFile, shell, shellArgs, interpreter := getScriptInterpreter(script, fileName)

	if interpreter {
		script = script[1:]
		offset++
	}

	var file, err = os.Create(scriptFile)

	if err != nil {
		return "", nil, "", "", err
	}

	if runtime.GOOS != "windows" { // && !interpreter {
		err = file.Chmod(0744)
	}

	defer file.Close()

	script = prepandEmptyLines(script, offset)

	for _, item := range script {
		fmt.Fprintln(file, item)
	}

	if err != nil {
		return "", nil, "", "", err
	}

	return fileName, arguments, shell, shellArgs, nil
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func prepandEmptyLines(script []string, offset int) []string {
	result := []string{}
	index := 0

	for ; index < len(script); index++ {
		item := script[index]

		if strings.TrimSpace(item) != "" {
			if strings.HasPrefix(item, "#!") {
				result = append(result, item)
				index++
			}
			break
		}
		result = append(result, item)
	}

	for ; offset > 0; offset-- {
		result = append(result, "")
	}

	for ; index < len(script); index++ {
		item := script[index]
		result = append(result, item)
	}

	return result
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (streamPostfix streamPostfix) Done() {
	if streamPostfix.content {
		fmt.Fprintf(streamPostfix.writer, streamPostfix.text)
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (streamPostfix *streamPostfix) Write(p []byte) (int, error) {
	streamPostfix.content = true

	return streamPostfix.writer.Write(p)
}
