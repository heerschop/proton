package main

import (
	"go/token"
	"go/types"
	"strconv"
)

func evaluateExpression(expression string) (bool, error) {
	fset := token.NewFileSet()

	result, error := types.Eval(fset, nil, token.NoPos, expression)

	if error != nil {
		return false, error
	}

	return strconv.ParseBool(result.Value.String())
}
