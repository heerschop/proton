package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strings"
)

type configType int

const (
	configElementBlock configType = iota
	configKeywordBlock
	configValue
	configLink
	configTag
	configError
)

type configRecord struct {
	Name     string
	Line     int
	Type     configType
	Content  []string
	Children []*configRecord
}
type configContent struct {
	Inherited int
	Offset    int
	Type      configType
	Content   []string
}

type keyValue struct {
	key   string
	value string
}

type schemaElement struct {
	Content  bool
	Children map[string]schemaElement
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func tokenParser(tokenChan chan elementToken, schema schemaElement) ([]*configRecord, error) {
	var err error

	root := configRecord{
		Name:     "root",
		Line:     0,
		Content:  make([]string, 0, 0),
		Children: make([]*configRecord, 0, 0),
	}

	func() {
		defer func() {
			recover := recover()

			if recover != nil {
				err = errors.New(recover.(string))
			}
		}()

		if !tokenProcessor(tokenChan, &root, schema) {
			token, _ := <-tokenChan

			panic(fmt.Sprintf("Unexpected end of file at line %d", token.Line))
		}
	}()

	return root.Children, err
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func tokenProcessor(tokenChan chan elementToken, item *configRecord, schema schemaElement) bool {

	comments := []string{}

	for {
		token, open := <-tokenChan
		contentComments := comments

		panicValue := token.Name

		if panicValue == "" {
			panicValue = token.Value
		}

		if !open || token.Type == tokenCloseBlock {
			return !open
		}

		if token.Type == tokenInvalid {
			panic(panicValue)
		}

		if token.Type == tokenComment {
			comments = append(comments, token.Value)
			continue
		}

		comments = []string{}

		if token.Type == tokenEmpty {
			continue
		}

		if configType, member, success := getSchemaMember(schema, token); success {
			child := &configRecord{
				Name:     token.Name,
				Line:     token.Line + 1,
				Type:     configType,
				Content:  nil,
				Children: nil,
			}

			if token.Type == tokenTag && member.Content && len(member.Children) > 0 {
				panic(fmt.Sprintf("Unexpected %s '%s' at line %d", tokenNames[token.Type], panicValue, token.Line+1))
			}

			if token.Type == tokenElementValue {
				if member, success := findChildElement(token.Name, schema); !success || !member.Content {
					panic(fmt.Sprintf("Unexpected %s '%s' at line %d", tokenNames[token.Type], panicValue, token.Line+1))
				}

				child.Content = []string{token.Value}
			}

			if token.Type == tokenElementLink {
				if _, success := findChildElement(token.Name, schema); !success { //Removed "|| member.Content" for enviroment link support
					panic(fmt.Sprintf("Unexpected %s '%s' at line %d", tokenNames[token.Type], panicValue, token.Line+1))
				}

				child.Content = []string{token.Value}
			}

			if token.Type == tokenKeywordValue {
				if member, success := schema.Children[token.Name]; !success || !member.Content {
					panic(fmt.Sprintf("Unexpected %s '%s' at line %d", tokenNames[token.Type], panicValue, token.Line+1))
				}

				child.Content = []string{token.Value}
			}

			if token.Type != tokenTag && child.Content == nil {
				tokenProcessor(tokenChan, child, member)
			}

			item.Children = append(item.Children, child)

			if (token.Type == tokenElementBlock || token.Type == tokenKeywordBlock) && len(child.Content) > 0 && (len(member.Children) > 0 || !member.Content) {
				panic(fmt.Sprintf("Unexpected content for %s '%s' at line %d", tokenNames[token.Type], panicValue, token.Line+1))
			}

			continue
		}

		if schema.Content {
			if token.Type == tokenContent {
				item.Content = append(item.Content, contentComments...)
				item.Content = append(item.Content, token.Value)

				continue
			}
			if token.Type == tokenElementValue || token.Type == tokenKeywordValue || token.Type == tokenElementLink || token.Type == tokenTag || token.Type == tokenContent {
				item.Content = append(item.Content, contentComments...)
				item.Content = append(item.Content, trimLeft(token.Raw, " ", -1))

				continue
			}
		}

		panic(fmt.Sprintf("Unexpected %s '%s' at line %d", tokenNames[token.Type], panicValue, token.Line+1))
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func loadSchema(schemaString string) schemaElement {
	var schema schemaElement

	bytes := []byte(configSchema)

	json.Unmarshal(bytes, &schema)

	return schema
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func getSchemaMember(schema schemaElement, token elementToken) (configType, schemaElement, bool) {
	configType := map[tokenType]configType{
		tokenElementBlock: configElementBlock,
		tokenKeywordBlock: configKeywordBlock,
		tokenElementValue: configValue,
		tokenKeywordValue: configValue,
		tokenElementLink:  configLink,
		tokenTag:          configTag,
	}

	if value, success := configType[token.Type]; success {
		if token.Type == tokenKeywordBlock || token.Type == tokenKeywordValue || token.Type == tokenTag {
			member, success := schema.Children[token.Name]
			return value, member, success
		}

		if member, success := findChildElement(token.Name, schema); success {
			_, success := schema.Children[token.Name]

			success = !success

			return value, member, success
		}

		return value, schemaElement{}, false
	}

	return 0, schemaElement{}, false

}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func findChildElement(name string, schema schemaElement) (schemaElement, bool) {
	for key, member := range schema.Children {
		if strings.HasPrefix(key, "^") && strings.HasSuffix(key, "$") {
			expression := regexp.MustCompile(key)

			if expression.MatchString(name) {
				return member, true
			}
		}
	}
	return schemaElement{}, false
}
