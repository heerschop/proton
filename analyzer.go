package main

import (
	"fmt"
	"regexp"
	"strings"
)

type tokenType int

const (
	tokenElementBlock tokenType = iota
	tokenKeywordBlock
	tokenCloseBlock
	tokenElementValue
	tokenKeywordValue
	tokenElementLink
	tokenContent
	tokenTag
	tokenComment
	tokenEmpty
	tokenInvalid
)

var tokenNames = []string{
	"ElementBlock",
	"KeywordBlock",
	"CloseBlock",
	"ElementValue",
	"KeywordValue",
	"ElementLink",
	"Content",
	"Tag",
	"Comment",
	"Empty",
	"Invalid",
}

type elementToken struct {
	Type  tokenType
	Line  int
	Name  string
	Value string
	Raw   string
}

var parsers = map[tokenType]parserFunction{
	tokenElementBlock: parseDefault,
	tokenKeywordBlock: parseDefault,
	tokenCloseBlock:   parseDefault,
	tokenElementValue: parseDefault,
	tokenKeywordValue: parseDefault,
	tokenElementLink:  parseDefault,
	tokenContent:      parseContent,
	tokenTag:          parseDefault,
	tokenComment:      parseDefault,
	tokenEmpty:        parseDefault,
}

type parserFunction func([]string, chan elementToken, tokenType, string, string, string, int) int

const namePattern = `[\w\s\-.,()&!:$]`

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func parseInvalid(tokenChan chan elementToken, line int, message string) {
	tokenChan <- elementToken{
		Type:  tokenInvalid,
		Line:  line,
		Value: message,
		Raw:   message,
	}

	panic(message)
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func parseDefault(lines []string, tokenChan chan elementToken, tokenType tokenType, name string, value string, raw string, index int) int {
	tokenChan <- elementToken{
		Type:  tokenType,
		Line:  index,
		Name:  name,
		Value: value,
		Raw:   raw,
	}

	return index + 1
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func parseContentLine(lines []string, indentLevel int, index int) (tokenType, string, string, int) {
	index++
	if index < len(lines) {
		line := lines[index]
		itemType, _, _ := parseLine(line)

		value := trimLeft(line, " ", indentLevel)

		return itemType, value, line, index
	}

	return tokenInvalid, "", "", index
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func parseContent(lines []string, tokenChan chan elementToken, tokenType tokenType, name string, value string, raw string, index int) int {

	if index == 0 && strings.HasPrefix(lines[index], "#!") {
		return parseDefault(lines, tokenChan, tokenComment, name, value, raw, index)
	}

	indentLevel := countFirst(lines[index], " ")
	emptyCount := 0

	for ; index < len(lines); tokenType, value, raw, index = parseContentLine(lines, indentLevel, index) {

		if tokenType == tokenEmpty {
			emptyCount++
			continue
		}

		if countFirst(lines[index], " ") < indentLevel {
			break
		}

		for ; emptyCount > 0; emptyCount-- {
			tokenChan <- elementToken{
				Type:  tokenContent,
				Line:  index - emptyCount,
				Name:  "",
				Value: "",
				Raw:   "",
			}
		}

		tokenChan <- elementToken{
			Type:  tokenContent,
			Line:  index,
			Name:  name,
			Value: value,
			Raw:   raw,
		}
	}

	return index
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func startAnalyzer(lines []string) chan elementToken {
	tokenChan := make(chan elementToken)

	go analyzer(lines, tokenChan)

	return tokenChan
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func parseCloseBlock(index int, indentLevel int, indent int, indentHistroy []int, tokenChan chan elementToken) ([]int, bool) {
	delta := indentLevel - indent
	length := len(indentHistroy) - 1

	if length < 0 || indentHistroy[length] != 0 && delta == 0 {
		return nil, false
	}

	for ; length >= 0; length-- {
		delta -= indentHistroy[length]
		indentHistroy = indentHistroy[:length]

		if delta < 0 {
			break
		}

		parseDefault(nil, tokenChan, tokenCloseBlock, "", "", "", index)

		if delta == 0 {
			return indentHistroy, true
		}
	}

	parseInvalid(tokenChan, index, fmt.Sprintf("Syntax error on line %d row indentation is invalid.", index+1))

	return nil, false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func analyzer(lines []string, tokenChan chan elementToken) {
	defer func() {
		close(tokenChan)
		recover()
	}()

	index := 0
	indentLevel := 0
	indentHistroy := []int{}

	for index < len(lines) {
		line := lines[index]

		tokenType, name, value := parseLine(strings.TrimSpace(line))

		if tokenType != tokenEmpty {
			indent := countFirst(line, " ")

			if result, success := parseCloseBlock(index, indentLevel, indent, indentHistroy, tokenChan); success {
				indentHistroy = result
				indentLevel = indent
			}
		}

		if tokenType == tokenElementBlock || tokenType == tokenKeywordBlock {
			indent := getIndentLevel(lines, index+1)
			indentLevel = countFirst(line, " ")
			delta := indent - indentLevel

			if delta < 0 {
				delta = 0
				indent = indentLevel
			}

			indentLevel = indent
			indentHistroy = append(indentHistroy, delta)
		}

		parser := parsers[tokenType]

		if parser == nil {
			parseInvalid(tokenChan, index, fmt.Sprintf("Syntax error on line %d '%s' not parseable.", index+1, tokenNames[tokenType]))
			continue
		}

		index = parser(lines, tokenChan, tokenType, name, value, line, index)
	}

	parseCloseBlock(index, indentLevel, 0, indentHistroy, tokenChan)
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func getIndentLevel(lines []string, index int) int {
	for ; index < len(lines); index++ {
		if len(strings.TrimSpace(lines[index])) > 0 {
			return countFirst(lines[index], " ")
		}
	}

	return 0
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func parseLine(line string) (tokenType, string, string) {
	value := strings.TrimSpace(line)

	if value == "" {
		return tokenEmpty, value, ""
	}

	if strings.HasPrefix(value, "#") {
		if strings.HasPrefix(value, "#!") {
			return tokenContent, "", value
		}

		return tokenComment, "", value
	}

	if tokenType, name, value, success := analyzeBlock(value); success {
		return tokenType, name, value
	}

	if tokenType, name, value, success := analyzeTag(value); success {
		return tokenType, name, value
	}

	if tokenType, name, value, success := analyzeOnliner(value); success {
		return tokenType, name, value
	}

	if tokenType, name, value, success := analyzeReference(value); success {
		return tokenType, name, value
	}

	return tokenContent, "", value
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func analyzeBlock(value string) (tokenType, string, string, bool) {
	var expression = regexp.MustCompile(`^(\w+|\"` + namePattern + `*\")\:\s*(#.*)?$`)

	match := expression.FindStringSubmatch(value)

	if len(match) == 3 {
		name := strings.Trim(match[1], "\"")

		if name != match[1] {
			return tokenElementBlock, name, "", true
		}

		return tokenKeywordBlock, name, "", true
	}

	return tokenInvalid, "", "", false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func analyzeTag(value string) (tokenType, string, string, bool) {
	expression := regexp.MustCompile(`^(\w+)\s*(#.*)?$`)

	match := expression.FindStringSubmatch(value)

	if len(match) == 3 {
		return tokenTag, match[1], "", true
	}

	return tokenInvalid, "", "", false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func analyzeOnliner(value string) (tokenType, string, string, bool) {
	expression := regexp.MustCompile(`^(\w+|\"` + namePattern + `*\"):\s+\"(.*)\"\s*(#.*)?$`)

	match := expression.FindStringSubmatch(value)

	if len(match) == 4 && !strings.Contains(strings.Replace(match[2], "\\\"", "", -1), "\"") {
		name := strings.Trim(match[1], "\"")

		if len(name) != len(match[1]) {
			return tokenElementValue, name, match[2], true
		}

		return tokenKeywordValue, name, match[2], true
	}

	return tokenInvalid, "", "", false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func analyzeReference(value string) (tokenType, string, string, bool) {
	expression := regexp.MustCompile(`^\"(` + namePattern + `*)\":\s+\<(` + namePattern + `*)\>\s*(#.*)?$`)

	match := expression.FindStringSubmatch(value)

	if len(match) == 4 {
		return tokenElementLink, match[1], match[2], true
	}

	return tokenInvalid, "", "", false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func removeStringBlocks(text string, stringIdentifier string) string {
	length := len(text)
	value := ""

	for index := 0; index < length; index++ {

		char := string(text[index])

		if char == stringIdentifier {
			for index++; index < length && string(text[index]) != stringIdentifier; index++ {
			}
			continue
		}

		value += char
	}

	return value
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func countParentheses(text string) int {
	value := text

	value = removeStringBlocks(value, "\"")
	value = removeStringBlocks(value, "'")
	value = removeStringBlocks(value, "`")

	count := 0

	count += strings.Count(value, "{")
	count -= strings.Count(value, "}")

	return count
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func countFirst(s string, sep string) int {
	value := strings.TrimLeft(s, sep)

	return len(s) - len(value)
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func trimLeft(s string, cutChar string, maxTrim int) string {
	result := strings.TrimLeft(s, cutChar)
	length := len(s) - len(result)

	if maxTrim == -1 {
		maxTrim = length
	}

	if length > maxTrim {
		length = length - maxTrim

		result = strings.Repeat(cutChar, length) + result
	}

	return result
}
