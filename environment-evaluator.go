package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"strings"
)

type environmentContext struct {
	Name        string
	Settings    map[string]map[string]configContent
	Templates   map[string]map[string]configContent
	Environment map[string]map[string]configContent
	Terms       []actionScript
	Error       string
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (context *environmentContext) getActionScript(name string, elements []*configRecord, scriptRoot string, settings map[string]map[string]configContent, templates map[string]map[string]configContent) actionScript {
	content, offset, error := getElementContent(name, elements, scriptRoot, settings, templates)

	if error != nil {
		context.Error = error.Error()

		return actionScript{}
	}

	return actionScript{name, offset, content}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func environmentCollector(elementTree []*configRecord, scriptRoot string) {

	elementWalker(elementTree, func(element *configRecord, context *elementContext) (bool, bool) {
		if context.data == nil {
			context.data = environmentContext{
				Settings:  mergeElementChildren(nil, elementTree, "settings", nil),
				Templates: mergeElementChildren(globalTemplates, elementTree, "templates", nil),
			}
		}

		environment := context.data.(environmentContext)
		environment.Settings = mergeElementChildren(environment.Settings, element.Children, "settings", nestedembers)
		environment.Templates = mergeElementChildren(environment.Templates, element.Children, "templates", nestedembers)
		context.data = environment

		if element.Name == "environment" {
			elementWalker(element.Children, func(element *configRecord, context *elementContext) (bool, bool) {
				content, error := mergeTemplates(element.Content, environment.Templates["environment"], element.Type == configElementBlock)
				element.Content = content

				if error != nil {
					element.Type = configError
					element.Content = []string{error.Error()}
				}

				if element.Type == configLink {
					resolveElementLink(element, elementTree, nil)
				}

				if element.Type == configElementBlock {
					shell := strings.Join(environment.Settings["environment"]["SHELL"].Content, "\n")

					element.Content = constructScript(element.Content, shell, scriptRoot)
				}

				return true, false
			})
			return false, false
		}

		return true, false
	})

}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func getNameLength(elementTree []*configRecord) int {
	nameLength := 10

	elementWalker(elementTree, func(element *configRecord, context *elementContext) (bool, bool) {
		if len(element.Name) > nameLength {
			nameLength = len(element.Name)
		}

		return false, false
	})

	return nameLength
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func environmentEvaluator(log io.Writer, elementTree []*configRecord, scriptRoot string, showSpinner bool) (map[string]map[string]configContent, int) {
	tempDir := newTempDir()

	removeFiles(tempDir + "*")

	nameLength := getNameLength(elementTree)
	errorMessage := ""

	executeScript("Evaluating.Environment scripts", nameLength, showSpinner, log, func(presenter presenter, outLogger io.Writer, errLogger io.Writer) (string, bool) {
		evaluationError := false

		presenter.start()

		elementWalker(elementTree, func(element *configRecord, context *elementContext) (bool, bool) {
			if context.data == nil {
				context.data = environmentContext{
					Environment: mergeElementChildren(nil, elementTree, "environment", nil),
					Settings:    mergeElementChildren(nil, elementTree, "settings", nil),
					Templates:   mergeElementChildren(globalTemplates, elementTree, "templates", nil),
				}
			}

			environment := context.data.(environmentContext)
			environment.Environment = mergeElementChildren(environment.Environment, element.Children, "environment", nestedembers)
			environment.Settings = mergeElementChildren(environment.Settings, element.Children, "settings", nestedembers)
			environment.Templates = mergeElementChildren(environment.Templates, element.Children, "templates", nestedembers)
			environment.Terms = appendActionScript(environment.Terms, environment.getActionScript("term", element.Children, scriptRoot, environment.Settings, environment.Templates))
			context.data = environment

			if element.Name == "environment" {

				for _, term := range environment.Terms {
					message, error := term.execute(tempDir, outLogger, errLogger, scriptRoot, environment.Environment["environment"])
					if error != nil {
						errorMessage = message
						return false, true
					}
				}

				elementWalker(element.Children, func(element *configRecord, context *elementContext) (bool, bool) {
					if element.Type == configElementBlock {
						if len(element.Content) > 0 {

							action := actionScript{element.Name, element.Line, element.Content}
							stringBuffer := new(bytes.Buffer)

							message, error := action.execute(tempDir, stringBuffer, stringBuffer, scriptRoot, environment.Environment["environment"])

							element.Content = strings.Split(strings.TrimSpace(stringBuffer.String()), "\n")

							if error != nil {
								errorMessage = message
								fmt.Fprintf(errLogger, "%s\n", stringBuffer.String())
								evaluationError = true
								element.Type = configError
								return true, false
							}

							if len(element.Content) == 1 {
								element.Type = configValue
							}
						}
					}

					return true, false
				})
				return false, false
			}

			return true, false
		})

		if !evaluationError {
			presenter.stop(ansiGreen, "", "Success")
			return "Success", true
		} else {
			presenter.stop(ansiRed, errorMessage, "Failed")
			return "Failed", false
		}
	})

	os.RemoveAll(tempDir)

	environment := mergeElementChildren(nil, elementTree, "environment", nil)

	return environment, nameLength

}
