// +build !windows

package main

var templateSimple = `#!proton

# This simple example demonstrates the basic functionality of
# proton by creating a single text file.

# To create this template
# run: proton --init simple > template-simple.sh

# To show the resulting actions scripts
# run: proton --show actions

settings:
    SHELL: "/bin/bash"

templates:
    init:
        set -e

"My Category":
    "My Configuration":
        detect:
            echo Script to detect the file.
            [ -f proton-example ]
        
        apply:
            echo Script to create the file.
            echo "hello proton" >  proton-example
        
        remove:
            echo Script to remove the file.
            rm proton-example`

var templateComplete = `#!proton

# This example demonstrates all the available constructs provided by proton. This
# configuration will create a single text file in tree different operating systems.

# To create this template
# run: proton --init complete > template-complete.sh

# To show the resulting actions scripts
# run: proton --show actions

present:               # Present can be used to show information in the users terminal.
    echo Proton $PROTON_VERSION complete template example.

settings:              # Settings can be used to define scope specific settings.
    SHELL:             # Specifies the default shell to use for this scope. When left empty, the terminal environment value SHELL will be used or the build in default for the specific OS.

templates:             # Templates are used for single or multiple line values. Their value is inserted at the place of their reference.
    "ShowMessage($action,$message)":
        echo "$GOOS => $action $message"
    "IsWindows()":
        #!proton-expression
        "$GOOS"=="windows"
    "IsMacOS()":
        #!proton-expression
        "$GOOS"=="darwin"
    "IsLinux()":
        #!proton-expression
        "$GOOS"=="linux"
    "FILE_NAME": "proton-example"

environment:           # Environment is used to define environment values. They can consist of a single value or a script block.
    "FILE_CONTENT":    # When a script block is used the result of the scriptblock will be used as the resulting environment value.
        echo "Proton is a standalone multi platform configuration tool. The configuration is hierarchical and"
        echo "idempotent by design. It's goal is to make system configuration as easy and flexible as possible."
        echo "No servers or infrastructure is needed, just one command and a config file. The target audience:"
        echo "developers who needs some basic system setup and IT professionals for configuring their"
        echo "infrastructure environment. Proton allows you to use any scripting language available on your"
        echo "computer and it is written in Go so it can be made available for any platform."

init:                  # Init can be used to run a script when starting proton
    echo Execute init actions.
final:                 # Final can be used to run a script when proton is completed
    echo Execute final actions.

"My Components":
    component          # Component is used to tag a category or an action to only execute when referenced
    "Bash":
        settings:
            SHELL: "/bin/bash"
        templates:
            init:
                set -e
            term:
                init: ""
        environment:
            "FILE_TILE": "Example Content of file: $FILE_NAME"
        detect:
            $ShowMessage('Detect','script to test the existence of file: $FILE_NAME')
            [ -f $FILE_NAME ]
        apply:
            $ShowMessage('Apply','script to create the file: $FILE_NAME')
            echo "$FILE_TILE"     > $FILE_NAME
            echo                 >> $FILE_NAME
            echo "$FILE_CONTENT" >> $FILE_NAME
        remove:
            $ShowMessage('Remove','script to remove the file: $FILE_NAME')
            rm $FILE_NAME

    "PowerShell":
        settings:
            SHELL: "powershell !.ps1"
        templates:
            init:
                $ErrorActionPreference=[Management.Automation.ActionPreference]::Stop
            term:
                init: ""
        environment:
            "FILE_TILE": "Example Content of file: $FILE_NAME"
        detect:
            $ShowMessage('Detect','script to test the existence of file: $FILE_NAME')
            $result=Test-Path $FILE_NAME
            [Environment]::Exit(!$result)
        apply:
            $ShowMessage('Apply','script to create the file: $FILE_NAME')
            Write-Output $env:FILE_TILE    | Out-File $FILE_NAME
            Write-Output ""                | Out-File $FILE_NAME -Append
            Write-Output $env:FILE_CONTENT | Out-File $FILE_NAME -Append
        remove:
            $ShowMessage('Remove','script to create the file: $FILE_NAME')
            Remove-Item $FILE_NAME

"Linux":
    term:
        $IsLinux()
    "Bash": <My Components.Bash>

"Linux Latent":
    latent
    templates:
        "FILE_NAME": "proton-example-latent"
    term:
        $IsLinux()
    "Bash": <My Components.Bash>

"MacOS":
    term:
        $IsMacOS()
    "Bash": <My Components.Bash>

"MacOS Latent":
    latent
    templates:
        "FILE_NAME": "proton-example-latent"
    term:
        $IsMacOS()
    "Bash": <My Components.Bash>
    
"Windows":
    term:
        $IsWindows()
    "PowerShell": <My Components.PowerShell>

"Windows Latent":
    latent
    templates:
        "FILE_NAME": "proton-example-latent"
    term:
        $IsWindows()
    "PowerShell": <My Components.PowerShell>`
