#!../proton

settings:
    SHELL: "/bin/bash"

init:
    go build ../

final:
    rm -f proton
    rm -f proton.exe

"Proton":
    environment:
        "PROTON_VERSION":
            echo $(./proton --version)

    "Linux x64":
        environment:
            "PACKAGE_NAME":
                echo "proton-$PROTON_VERSION-linux-x64.tar.gz"
        detect:
            set -e
            
            hash=$(cat proton-linux.sha1)
            hash=${hash:0:64}

            echo $hash proton | sha256sum -c -

            [ -f $PACKAGE_NAME ]
            
            echo Detected: $PROTON_VERSION
        apply:
            set -e
            echo Creating new Linux x64 release version $PROTON_VERSION

            tar -cvzf $PACKAGE_NAME proton

            sha256sum proton > proton-linux.sha1

            echo Created: $PROTON_VERSION
        remove:
            rm $PACKAGE_NAME
            rm proton-linux.sha1

    "Darwin x64":
        environment:
            "PACKAGE_NAME":
                echo "proton-$PROTON_VERSION-darwin-x64.tar.gz"
        detect:
            set -e
            env GOOS=darwin GOARCH=amd64 go build -v ../

            hash=$(cat proton-darwin.sha1)
            hash=${hash:0:64}

            echo $hash proton | sha256sum -c -

            [ -f $PACKAGE_NAME ]
            
            echo Detected: $PROTON_VERSION
        apply:
            set -e
            echo Creating new Darwin x64 release version $PROTON_VERSION

            tar -cvzf $PACKAGE_NAME proton

            sha256sum proton > proton-darwin.sha1

            echo Created: $PROTON_VERSION
        remove:
            rm $PACKAGE_NAME
            rm proton-darwin.sha1

    "Windows x64":
        environment:
            "PACKAGE_NAME":
                echo "proton-$PROTON_VERSION-windows-x64.zip"
        detect:
            set -e
            env GOOS=windows GOARCH=amd64 go build -v ../

            hash=$(cat proton-windows.sha1)
            hash=${hash:0:64}

            echo $hash proton | sha256sum -c -

            [ -f $PACKAGE_NAME ]

            echo Detected: $PROTON_VERSION
        apply:
            set -e
            echo Creating new Windows x64 release version $PROTON_VERSION

            zip -r $PACKAGE_NAME proton.exe

            sha256sum proton > proton-windows.sha1

            echo Created: $PROTON_VERSION
        remove:
            rm $PACKAGE_NAME
            rm proton-windows.sha1
