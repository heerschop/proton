// +build !windows

package main

var ansiRed = "\033[0;31m"
var ansiGreen = "\033[0;32m"
var ansiYellow = "\033[0;33m"
var ansiBlue = "\033[0;94m"
var ansiOrange = "\033[38;2;255;165;0m"
var ansiBold = "\033[1m"
var ansiItalic = "\033[3m"
var ansiDim = "\033[2m"
var ansiNormal = "\033[0m"

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func disableAnsi() {
	ansiRed = ""
	ansiGreen = ""
	ansiYellow = ""
	ansiBlue = ""
	ansiOrange = ""
	ansiBold = ""
	ansiItalic = ""
	ansiDim = ""
	ansiNormal = ""

	contentPrefix = "- "
}
