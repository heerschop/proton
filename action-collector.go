package main

import (
	"errors"
	"os"
	"os/exec"
	"path"
	"reflect"
	"regexp"
	"runtime"
	"strings"
)

type actionTag struct {
	offset int
	value  bool
}

type actionConfig struct {
	offset      int
	latent      actionTag
	component   actionTag
	name        string
	settings    map[string]map[string]configContent
	templates   map[string]map[string]configContent
	environment map[string]map[string]configContent
	terms       []actionScript
	detect      actionScript
	apply       actionScript
	remove      actionScript
	error       string
}

type actionScript struct {
	name   string
	offset int
	script []string
}

var globalMembers = []string{
	"_top_", // action links resolver, only resolves top level templates
	"present",
	"init",
	"final",
	"term",
	"detect",
	"apply",
	"remove",
	"environment",
}

var nestedembers = []string{
	"_top_", // action links resolver, only resolves top level templates
	"term",
	"detect",
	"apply",
	"remove",
	"environment",
}

var jsonNamePattern = "\"^" + strings.Replace(namePattern, "\\", "\\\\", -1) + "*$\""

var templateName = `^\w+(\((\$\w+)?(,\$\w+\s?)*\))?$`

var jsonTemplateName = strings.Replace(templateName, "\\", "\\\\", -1)

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
var globalTemplates = createElementContent([]keyValue{
	keyValue{
		key:   "GOOS",
		value: runtime.GOOS,
	},
	keyValue{
		key:   "PROTON_VERSION",
		value: protonVersion,
	},
})

var configSchema = `
{
	"Content": false,
	"Children": {
		"present": { "Content": true, "Children": {} },
		"init": { "Content": true, "Children": {} },
		"final": { "Content": true, "Children": {} },
		"settings": { 
			"Content": false,
			"Children": {
				"SHELL": { "Content": true, "Children": {} },
				"environment": { 
					"Content": false,
					"Children": {
						"SHELL": { "Content": true, "Children": {} }
					}
				},
				"init": { 
					"Content": false,
					"Children": {
						"SHELL": { "Content": true, "Children": {} }
					}
				},
				"final": { 
					"Content": false,
					"Children": {
						"SHELL": { "Content": true, "Children": {} }
					}
				},
				"term": { 
					"Content": false,
					"Children": {
						"SHELL": { "Content": true, "Children": {} }
					}
				},
				"detect": { 
					"Content": false,
					"Children": {
						"SHELL": { "Content": true, "Children": {} }
					}
				},
				"apply": { 
					"Content": false,
					"Children": {
						"SHELL": { "Content": true, "Children": {} }
					}
				},
				"remove": { 
					"Content": false,
					"Children": {
						"SHELL": { "Content": true, "Children": {} }
					}
				}
			}
		},
		"templates": { 
			"Content": false,
			"Children": {
				"init":  { "Content": true, "Children": {} },
				"final": { "Content": true, "Children": {} },
				"` + jsonTemplateName + `":{ "Content": true, "Children": {} },
				"environment": { 
					"Content": false,
					"Children": {
						"init":  { "Content": true, "Children": {} },
						"final": { "Content": true, "Children": {} },
						"` + jsonTemplateName + `":{ "Content": true, "Children": {} }
					}
				},
				"term": { 
					"Content": false,
					"Children": {
						"init":  { "Content": true, "Children": {} },
						"final": { "Content": true, "Children": {} },
						"` + jsonTemplateName + `":{ "Content": true, "Children": {} }
					}
				},
				"detect": { 
					"Content": false,
					"Children": {
						"init":  { "Content": true, "Children": {} },
						"final": { "Content": true, "Children": {} },
						"` + jsonTemplateName + `":{ "Content": true, "Children": {} }
					}
				},
				"apply": { 
					"Content": false,
					"Children": {
						"init":  { "Content": true, "Children": {} },
						"final": { "Content": true, "Children": {} },
						"` + jsonTemplateName + `":{ "Content": true, "Children": {} }
					}
				},
				"remove": { 
					"Content": false,
					"Children": {
						"init":  { "Content": true, "Children": {} },
						"final": { "Content": true, "Children": {} },
						"` + jsonTemplateName + `":{ "Content": true, "Children": {} }
					}
				}
			}
		},
		"environment": { 
			"Content": false,
			"Children": {
				"^\\w+$":{ "Content": true, "Children": {} },
				"init": { 
					"Content": false,
					"Children": {
						"^\\w+$":{ "Content": true, "Children": {} }
					}
				},
				"final": { 
					"Content": false,
					"Children": {
						"^\\w+$":{ "Content": true, "Children": {} }
					}
				},
				"term": { 
					"Content": false,
					"Children": {
						"^\\w+$":{ "Content": true, "Children": {} }
					}
				},
				"detect": { 
					"Content": false,
					"Children": {
						"^\\w+$":{ "Content": true, "Children": {} }
					}
				},
				"apply": { 
					"Content": false,
					"Children": {
						"^\\w+$":{ "Content": true, "Children": {} }
					}
				},
				"remove": { 
					"Content": false,
					"Children": {
						"^\\w+$":{ "Content": true, "Children": {} }
					}
				}
			}
		},
		` + jsonNamePattern + `: {
			"Content": false,
			"Children": {
				"latent": { "Content": false, "Children": {} },
				"component": { "Content": false, "Children": {} },
				"settings": { 
					"Content": false,
					"Children": {
						"SHELL": { "Content": true, "Children": {} },
						"environment": { 
							"Content": false,
							"Children": {
								"SHELL": { "Content": true, "Children": {} }
							}
						},
						"term": { 
							"Content": false,
							"Children": {
								"SHELL": { "Content": true, "Children": {} }
							}
						},
						"detect": { 
							"Content": false,
							"Children": {
								"SHELL": { "Content": true, "Children": {} }
							}
						},
						"apply": { 
							"Content": false,
							"Children": {
								"SHELL": { "Content": true, "Children": {} }
							}
						},
						"remove": { 
							"Content": false,
							"Children": {
								"SHELL": { "Content": true, "Children": {} }
							}
						}
					}
				},
				"templates": { 
					"Content": false,
					"Children": {
						"init":  { "Content": true, "Children": {} },
						"final": { "Content": true, "Children": {} },
						"` + jsonTemplateName + `":{ "Content": true, "Children": {} },
						"environment": { 
							"Content": false,
							"Children": {
								"init":  { "Content": true, "Children": {} },
								"final": { "Content": true, "Children": {} },
								"` + jsonTemplateName + `":{ "Content": true, "Children": {} }
							}
						},
						"term": { 
							"Content": false,
							"Children": {
								"init":  { "Content": true, "Children": {} },
								"final": { "Content": true, "Children": {} },
								"` + jsonTemplateName + `":{ "Content": true, "Children": {} }
							}
						},
						"detect": { 
							"Content": false,
							"Children": {
								"init":  { "Content": true, "Children": {} },
								"final": { "Content": true, "Children": {} },
								"` + jsonTemplateName + `":{ "Content": true, "Children": {} }
							}
						},
						"apply": { 
							"Content": false,
							"Children": {
								"init":  { "Content": true, "Children": {} },
								"final": { "Content": true, "Children": {} },
								"` + jsonTemplateName + `":{ "Content": true, "Children": {} }
							}
						},
						"remove": { 
							"Content": false,
							"Children": {
								"init":  { "Content": true, "Children": {} },
								"final": { "Content": true, "Children": {} },
								"` + jsonTemplateName + `":{ "Content": true, "Children": {} }
							}
						}
					}
				},
				"environment": { 
					"Content": false,
					"Children": {
						"^\\w+$":{ "Content": true, "Children": {} },
						"term": { 
							"Content": false,
							"Children": {
								"^\\w+$":{ "Content": true, "Children": {} }
							}
						},
						"detect": { 
							"Content": false,
							"Children": {
								"^\\w+$":{ "Content": true, "Children": {} }
							}
						},
						"apply": { 
							"Content": false,
							"Children": {
								"^\\w+$":{ "Content": true, "Children": {} }
							}
						},
						"remove": { 
							"Content": false,
							"Children": {
								"^\\w+$":{ "Content": true, "Children": {} }
							}
						}
					}
				},
				"term": { "Content": true, "Children": {} },
				` + jsonNamePattern + `: {
					"Content": false,
					"Children": {
						"latent": { "Content": false, "Children": {} },
						"component": { "Content": false, "Children": {} },
						"settings": { 
							"Content": false,
							"Children": {
								"SHELL": { "Content": true, "Children": {} },
								"environment": { 
									"Content": false,
									"Children": {
										"SHELL": { "Content": true, "Children": {} }
									}
								},
								"term": { 
									"Content": false,
									"Children": {
										"SHELL": { "Content": true, "Children": {} }
									}
								},
								"detect": { 
									"Content": false,
									"Children": {
										"SHELL": { "Content": true, "Children": {} }
									}
								},
								"apply": { 
									"Content": false,
									"Children": {
										"SHELL": { "Content": true, "Children": {} }
									}
								},
								"remove": { 
									"Content": false,
									"Children": {
										"SHELL": { "Content": true, "Children": {} }
									}
								}
							}
						},
						"templates": { 
							"Content": false,
							"Children": {
								"init":  { "Content": true, "Children": {} },
								"final": { "Content": true, "Children": {} },
								"` + jsonTemplateName + `":{ "Content": true, "Children": {} },
								"environment": { 
									"Content": false,
									"Children": {
										"init":  { "Content": true, "Children": {} },
										"final": { "Content": true, "Children": {} },
										"` + jsonTemplateName + `":{ "Content": true, "Children": {} }
									}
								},
								"term": { 
									"Content": false,
									"Children": {
										"init":  { "Content": true, "Children": {} },
										"final": { "Content": true, "Children": {} },
										"` + jsonTemplateName + `":{ "Content": true, "Children": {} }
									}
								},
								"detect": { 
									"Content": false,
									"Children": {
										"init":  { "Content": true, "Children": {} },
										"final": { "Content": true, "Children": {} },
										"` + jsonTemplateName + `":{ "Content": true, "Children": {} }
									}
								},
								"apply": { 
									"Content": false,
									"Children": {
										"init":  { "Content": true, "Children": {} },
										"final": { "Content": true, "Children": {} },
										"` + jsonTemplateName + `":{ "Content": true, "Children": {} }
									}
								},
								"remove": { 
									"Content": false,
									"Children": {
										"init":  { "Content": true, "Children": {} },
										"final": { "Content": true, "Children": {} },
										"` + jsonTemplateName + `":{ "Content": true, "Children": {} }
									}
								}
							}
						},
						"environment": { 
							"Content": false,
							"Children": {
								"^\\w+$":{ "Content": true, "Children": {} },
								"term": { 
									"Content": false,
									"Children": {
										"^\\w+$":{ "Content": true, "Children": {} }
									}
								},
								"detect": { 
									"Content": false,
									"Children": {
										"^\\w+$":{ "Content": true, "Children": {} }
									}
								},
								"apply": { 
									"Content": false,
									"Children": {
										"^\\w+$":{ "Content": true, "Children": {} }
									}
								},
								"remove": { 
									"Content": false,
									"Children": {
										"^\\w+$":{ "Content": true, "Children": {} }
									}
								}
							}
						},
						"term":  { "Content": true,  "Children": {} },
						"detect": { "Content": true,  "Children": {} },
						"apply": { "Content": true,  "Children": {} },
						"remove": { "Content": true,  "Children": {} }
					}
				}
			}
		}
	}
}
`

type elementContext struct {
	name  string
	data  interface{}
	depth int
}

type findElementResult struct {
	element *configRecord
	context *elementContext
}

type actionContextData struct {
	elementTree []*configRecord
	scriptRoot  string
}

type enterElementFunction func(member *configRecord, context *elementContext) (bool, bool)

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func elementWalkerProcessor(currentTree []*configRecord, context *elementContext, enterElementFunction enterElementFunction) {

	for _, element := range currentTree {
		clonedContext := *context
		clonedContext.name = joinStrings([]string{context.name, element.Name}, ".")

		nest, stop := enterElementFunction(element, &clonedContext)

		if stop {
			return
		}

		if nest {
			clonedContext.depth++
			elementWalkerProcessor(element.Children, &clonedContext, enterElementFunction)
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func elementWalker(elementTree []*configRecord, enterElementFunction enterElementFunction) {
	context := &elementContext{}

	if enterElementFunction == nil {
		enterElementFunction = func(member *configRecord, context *elementContext) (bool, bool) { return true, false }
	}

	elementWalkerProcessor(elementTree, context, enterElementFunction)
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func resolveElementLink(element *configRecord, configTree []*configRecord, enterElementFunction enterElementFunction) (elementContext, bool) {

	if enterElementFunction == nil {
		enterElementFunction = func(member *configRecord, context *elementContext) (bool, bool) { return true, false }
	}

	if len(element.Content) == 0 || len(strings.TrimSpace(element.Content[0])) == 0 {
		element.Type = configError
		element.Content = []string{"Element reference <> is empty."}

		return elementContext{}, false
	}

	if len(element.Content) > 1 {
		element.Type = configError
		element.Content = []string{"Only one content line support for a element reference."}

		return elementContext{}, false
	}
	if strings.HasPrefix(element.Content[0], "<") && strings.HasSuffix(element.Content[0], ">") {
		element.Type = configError
		element.Content = []string{"Circular element " + element.Content[0] + " reference detected."}

		return elementContext{}, false
	}

	if len(element.Children) > 0 {
		element.Type = configError
		element.Content = []string{"Element references do not support children."}

		return elementContext{}, false
	}

	found := findElement(element.Content[0], configTree, enterElementFunction)

	if len(found) == 0 {
		element.Type = configError
		element.Content = []string{"Element reference <" + element.Content[0] + "> not found."}

		return elementContext{}, false
	}

	if len(found) > 1 {
		element.Type = configError
		element.Content = []string{"Multiple elements found for reference <" + element.Content[0] + ">."}

		return elementContext{}, false
	}
	if found[0].element.Type == configLink {
		element.Content = []string{"<" + element.Content[0] + ">"}
		resolveElementLink(found[0].element, configTree, enterElementFunction)
	}

	// if found[0].element.Type == configLink {
	// 	element.Type = configError
	// 	element.Content = []string{"Recursive element references are not supported."}

	// 	return elementContext{}, false
	// }

	element.Type = found[0].element.Type
	element.Content = found[0].element.Content
	element.Children = found[0].element.Children

	return *found[0].context, true
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
type actionContext struct {
	Environment map[string]map[string]configContent
	Settings    map[string]map[string]configContent
	Templates   map[string]map[string]configContent
	Terms       []actionScript
	Component   actionTag
	Latent      actionTag
	Error       string
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func joinStrings(array []string, sep string) string {
	var result []string

	for _, item := range array {
		if item != "" {
			result = append(result, item)
		}
	}

	return strings.Join(result, ".")
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func appendActionScript(scripts []actionScript, script actionScript) []actionScript {
	if len(script.script) == 0 {
		return scripts[0:]
	}

	return append(scripts, script)
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func createElementContent(keyValueList []keyValue) map[string]map[string]configContent {
	result := map[string]map[string]configContent{}

	for _, keyValue := range keyValueList {
		addElementContent(result, keyValue)
	}

	return result
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func addElementContent(target map[string]map[string]configContent, keyValue keyValue) {
	for _, item := range globalMembers {
		if target[item] == nil {
			target[item] = map[string]configContent{}
		}

		target[item][keyValue.key] = configContent{
			Inherited: 0,
			Offset:    -1,
			Type:      configValue,
			Content:   []string{keyValue.value},
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (actionContextData actionContextData) merge(element *configRecord, context *elementContext) (bool, bool) {
	if context.data == nil {
		context.data = actionContext{
			Environment: mergeElementChildren(nil, actionContextData.elementTree, "environment", nil),
			Settings:    mergeElementChildren(nil, actionContextData.elementTree, "settings", nil),
			Templates:   mergeElementChildren(globalTemplates, actionContextData.elementTree, "templates", nil),
		}
	}

	action := context.data.(actionContext)

	action.Environment = mergeElementChildren(action.Environment, element.Children, "environment", nestedembers)
	action.Settings = mergeElementChildren(action.Settings, element.Children, "settings", nestedembers)
	action.Templates = mergeElementChildren(action.Templates, element.Children, "templates", nestedembers)
	action.Terms = appendActionScript(action.Terms, action.getActionScript("term", element.Children, actionContextData.scriptRoot, action.Settings, action.Templates))

	if tag := getActionTag(element.Children, "component"); tag.value {
		action.Component = tag
	}

	if tag := getActionTag(element.Children, "latent"); tag.value {
		action.Latent = tag
	}

	context.data = action

	return true, false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (context actionContext) merge(source actionContext) actionContext {
	for memberName, members := range source.Environment {
		for itemName, item := range members {
			context.Environment[memberName][itemName] = item
		}
	}
	for memberName, members := range source.Settings {
		for itemName, item := range members {
			context.Settings[memberName][itemName] = item
		}
	}
	for memberName, members := range source.Templates {
		for itemName, item := range members {
			context.Templates[memberName][itemName] = item
		}
	}

	for _, term := range source.Terms {
		context.Terms = appendActionScript(context.Terms, term)
	}

	context.Component = source.Component
	context.Latent = source.Latent

	return context
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func globalCollector(elementTree []*configRecord, scriptRoot string) (configContent, configContent, configContent, map[string]map[string]configContent, map[string]map[string]configContent) {

	settings := mergeElementChildren(nil, elementTree, "settings", nil)
	templates := mergeElementChildren(globalTemplates, elementTree, "templates", nil)

	present := getActionConfigScript("present", elementTree, scriptRoot, settings, templates)
	init := getActionConfigScript("init", elementTree, scriptRoot, settings, templates)
	final := getActionConfigScript("final", elementTree, scriptRoot, settings, templates)

	return present, init, final, settings, templates
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func actionCollector(elementTree []*configRecord, scriptRoot string) []actionConfig {

	actionContextData := actionContextData{
		elementTree: elementTree,
		scriptRoot:  scriptRoot,
	}

	actions := []actionConfig{}

	elementWalker(elementTree, func(element *configRecord, context *elementContext) (bool, bool) {
		actionContextData.merge(element, context)

		action := context.data.(actionContext)

		if element.Type == configLink {

			content, error := mergeTemplates(element.Content, action.Templates["_top_"], element.Type == configElementBlock)
			element.Content = content

			if error != nil {
				element.Type = configError
				element.Content = []string{error.Error()}
			} else {
				if matchContext, success := resolveElementLink(element, elementTree, actionContextData.merge); success {
					matchAction := matchContext.data.(actionContext)
					context.data = matchAction.merge(context.data.(actionContext))
					action = context.data.(actionContext)
				}
			}

		}

		if element.Type == configError {
			actions = append(actions, actionConfig{
				name:   context.name,
				offset: element.Line,
				error:  element.Content[0],
			})
		}

		if element.Type != configElementBlock {
			return false, false
		}

		if context.depth != 1 {
			return true, false
		}

		actionConfig := actionConfig{
			offset:      element.Line,
			latent:      action.Latent,
			component:   action.Component,
			name:        context.name,
			settings:    action.Settings,
			templates:   action.Templates,
			environment: action.Environment,
			terms:       action.Terms,
			detect:      action.getActionScript("detect", element.Children, scriptRoot, action.Settings, action.Templates),
			apply:       action.getActionScript("apply", element.Children, scriptRoot, action.Settings, action.Templates),
			remove:      action.getActionScript("remove", element.Children, scriptRoot, action.Settings, action.Templates),
			error:       action.Error,
		}

		actions = append(actions, actionConfig)

		return false, false
	})

	return actions
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func getActionConfigScript(name string, elements []*configRecord, scriptRoot string, settings map[string]map[string]configContent, templates map[string]map[string]configContent) configContent {
	content, offset, error := getElementContent(name, elements, scriptRoot, settings, templates)

	if error != nil {
		return configContent{
			Type:    configError,
			Offset:  offset,
			Content: []string{error.Error()},
		}
	}
	return configContent{
		Type:    configKeywordBlock,
		Offset:  offset,
		Content: content,
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (context *actionContext) getActionScript(name string, elements []*configRecord, scriptRoot string, settings map[string]map[string]configContent, templates map[string]map[string]configContent) actionScript {
	content, offset, error := getElementContent(name, elements, scriptRoot, settings, templates)

	if error != nil {
		context.Error = error.Error()

		return actionScript{}
	}

	return actionScript{name, offset, content}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func (configRecord *configRecord) getElement(name string) (*configRecord, bool) {
	for _, element := range configRecord.Children {
		if element.Name == name {
			return element, true
		}
	}
	return nil, false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func findElementOld(name string, configTree []*configRecord) []*configRecord {
	result := []*configRecord{}

	for _, element := range configTree {
		if strings.HasPrefix(name, element.Name+".") {

			name := strings.TrimPrefix(name, element.Name+".")

			result = append(result, findElementOld(name, element.Children)...)
		}
		if name == element.Name {
			result = append(result, element)
		}
	}

	return result
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func findElement(name string, elementTree []*configRecord, enterElementFunction enterElementFunction) []findElementResult {
	result := []findElementResult{}

	elementWalker(elementTree, func(element *configRecord, context *elementContext) (bool, bool) {

		enterElementFunction(element, context)

		if context.name == name {
			result = append(result, findElementResult{
				element: element,
				context: context,
			})
		}

		return true, false
	})

	return result
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func stringInSlice(s string, list []string) bool {
	for _, item := range list {
		if item == s {
			return true
		}
	}
	return false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func getElementContent(name string, elements []*configRecord, scriptRoot string, settings map[string]map[string]configContent, templates map[string]map[string]configContent) ([]string, int, error) {
	shell := strings.Join(settings[name]["SHELL"].Content, "\n")

	for _, element := range elements {
		if element.Name == name {
			if len(element.Content) == 0 {
				return nil, element.Line, nil
			}

			content, error := mergeTemplates(element.Content, templates[name], true)

			if error == nil {
				content = constructScript(content, shell, scriptRoot)
			}

			return content, element.Line, error
		}
	}

	return nil, -1, nil

}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func isEmpty(array []string) bool {
	for _, item := range array {
		if strings.TrimSpace(item) != "" {
			return false
		}
	}

	return true
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func mergeTemplates(content []string, templates map[string]configContent, initFinal bool) ([]string, error) {

	if len(content) == 0 {
		return content, nil
	}

	contentString := strings.Join(content, "\n")

	resolveCount := 0

	for true {
		lastEvaluated := ""

		for key, value := range templates {
			content, evaluated, error := evaluateTemplate(contentString, key, value.Content)

			if error != nil {
				return nil, error
			}

			contentString = content

			if evaluated > 0 {
				lastEvaluated = key
			}
		}

		if len(contentString) == 0 {
			return []string{}, nil
		}

		if lastEvaluated == "" {
			break
		}

		resolveCount++

		if resolveCount > len(templates) {
			return nil, errors.New("Circular template reference '" + lastEvaluated + "' detected.")
		}
	}

	content = strings.Split(contentString, "\n")

	if initFinal {
		if template, success := templates["init"]; success && !isEmpty(template.Content) {
			if strings.HasPrefix(content[0], "#!") {
				header := append([]string{}, content[0])
				content = append(template.Content, content[1:]...)
				content = append(header, content...)
			} else {
				content = append(template.Content, content...)
			}
		}

		if template, success := templates["final"]; success && !isEmpty(template.Content) {
			content = append(content, template.Content...)
		}
	}

	return content, nil
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func constructScript(content []string, shell string, scriptRoot string) []string {
	if len(content) == 0 {
		return content
	}

	if shell == "" {
		shell = os.Getenv("SHELL")
		if shell == "" {
			shell = "/bin/bash"

			if runtime.GOOS == "windows" {
				shell = "cmd /q /c !.cmd"
			}
		}
	}

	if !strings.HasPrefix(content[0], "#!") {
		var header []string

		header = append(header, "#!"+shell)

		content = append(header, content...)
	}

	content[0] = os.ExpandEnv(content[0])

	expression := regexp.MustCompile(`^#!((\".*?\")|(.*?))\s*(\s.*)?$`)

	matches := expression.FindStringSubmatch(content[0])

	if len(matches) == 5 {
		quotes := ""

		fileName := strings.Trim(matches[1], "\"")

		if fileName != matches[1] {
			quotes = "\""
		}

		fileName = strings.Replace(fileName, "\\", "/", -1)

		if _, err := exec.LookPath(fileName); err != nil {

			fileName = path.Join(scriptRoot, fileName)
		}

		if exists(fileName) {
			content[0] = "#!" + quotes + fileName + quotes + matches[4]
		}
	}

	return content
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func exists(path string) bool {
	_, err := os.Stat(path)

	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}

	return os.IsExist(err)
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func replace(content string, pattern string, value string) string {
	pattern = regexp.QuoteMeta(pattern)

	expression := regexp.MustCompile(pattern + `([^w]|$)`)
	value = strings.Replace(value, "$", "$$", -1)

	return expression.ReplaceAllString(content, value+"$1")
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func getActionTag(elements []*configRecord, name string) actionTag {
	for _, element := range elements {
		if element.Name == name {
			return actionTag{element.Line, true}
		}
	}

	return actionTag{-1, false}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func mergeChildren(target map[string]configContent, source map[string]map[string]configContent) map[string]configContent {
	result := map[string]configContent{}

	for key, value := range target {
		result[key] = configContent{
			Inherited: value.Inherited + 1,
			Type:      value.Type,
			Content:   value.Content,
			Offset:    value.Offset,
		}
	}

	for key, contentMap := range source {
		if content, success := contentMap[key]; success {
			if !contains(schemaKeywords, key) || len(content.Content) > 0 {
				result[key] = content
			}
		}
	}

	return result
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func mergeContent(target map[string]configContent, source map[string]configContent) map[string]configContent {
	result := map[string]configContent{}

	for key, value := range target {
		result[key] = value
	}

	for key, content := range source {
		if !contains(schemaKeywords, key) || len(content.Content) > 0 {
			result[key] = content
		}
	}

	return result
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func mergeElementChildren(target map[string]map[string]configContent, elements []*configRecord, name string, members []string) map[string]map[string]configContent {
	current := map[string]map[string]configContent{}

	if members == nil {
		members = globalMembers
	}

	for _, element := range elements {
		if element.Name == name {
			for _, member := range element.Children {
				children := map[string]configContent{}

				children[member.Name] = configContent{
					Inherited: 0,
					Type:      member.Type,
					Content:   member.Content,
					Offset:    member.Line,
				}

				for _, child := range member.Children {
					children[child.Name] = configContent{
						Inherited: 0,
						Type:      child.Type,
						Content:   child.Content,
						Offset:    member.Line,
					}
				}

				current[member.Name] = children
			}
		}
	}

	result := map[string]map[string]configContent{}

	//members, _ = remove(members, name)

	for _, key := range members {
		result[key] = mergeChildren(target[key], current)
		result[key] = mergeContent(result[key], current[key])
	}

	return result
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func splitTemplate(text string) (template string, name string, arguments []string, success bool) {
	expression := regexp.MustCompile(`^((\w+)(\((\$\w+)?(,\$\w+\s?)*\))?)$`)

	matches := expression.FindAllStringSubmatch(text, -1)

	if len(matches) >= 1 && len(matches[0]) >= 3 {
		template := matches[0][1]
		name := matches[0][2]
		arguments := []string{}

		for _, item := range strings.Split(strings.Trim(matches[0][3], "()"), ",") {
			if len(strings.TrimSpace(item)) > 0 {
				arguments = append(arguments, item)
			}
		}

		return name, template, arguments, true
	}

	return "", "", nil, false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func findTemplate(templateName string, content string) (template string, name string, arguments []string, success bool) {
	expression := regexp.MustCompile(`.*?(\$(` + templateName + `)(\(\$?(\w*|\'.*\')(,\$?(\w+|\'.+\')\s?)*\))?).*?`)
	matches := expression.FindAllStringSubmatch(content, -1)

	if len(matches) >= 1 && len(matches[0]) >= 5 {
		template := matches[0][1]
		name := matches[0][2]
		arguments := []string{}

		for _, item := range strings.Split(strings.Trim(matches[0][3], "()"), ",") {
			if len(strings.TrimSpace(item)) > 0 {
				arguments = append(arguments, strings.Trim(item, "'"))
			}
		}

		return name, template, arguments, true
	}

	return "", "", nil, false
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func evaluateTemplate(contentString string, templateName string, templateContent []string) (string, int, error) {
	count := 0
	name, templateSyntax, templateArguments, success := splitTemplate(templateName)

	if !success {
		return "", count, errors.New("Template syntax '" + templateName + "' is invalid.")
	}

	templateString := strings.Join(templateContent, "\n")

	if _, contentSyntax, _, success := findTemplate(name, templateString); success {
		return "", 0, errors.New("Circular template reference '" + contentSyntax + "' detected.")
	}

	for true {
		_, contentSyntax, contentArguments, success := findTemplate(name, contentString)

		if !success {
			break
		}

		count++

		if len(contentArguments) > len(templateArguments) {
			return "", count, errors.New("Too many arguments for template call: " + templateSyntax)
		}
		if len(contentArguments) < len(templateArguments) {
			return "", count, errors.New("Not enough arguments for template call: " + templateSyntax)
		}
		if len(templateArguments) == 0 && "$"+templateSyntax != contentSyntax {
			return "", count, errors.New("Invalid syntax for template call: " + templateSyntax)
		}

		content := templateString

		for index, item := range templateArguments {
			content = strings.Replace(content, item, contentArguments[index], -1)
		}

		contentString = replace(contentString, contentSyntax, content)
	}

	return contentString, count, nil
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
func mapCopy(dst, src interface{}) {
	dv, sv := reflect.ValueOf(dst), reflect.ValueOf(src)

	for _, k := range sv.MapKeys() {
		dv.SetMapIndex(k, sv.MapIndex(k))
	}
}
