# Repository cleanup

### List repository files by size
```
git rev-list --objects --all \
| git cat-file --batch-check='%(objecttype) %(objectname) %(objectsize) %(rest)' \
| sed -n 's/^blob //p' \
| sort --numeric-sort --key=2 \
| numfmt --field=2 --to=iec-i --suffix=B --padding=7 --round=nearest
```

### Remove file from repository history
```
FILE_PATH=<<FILE-TO-REMOVE>>
git filter-branch --force --index-filter      \
"git rm --cached --ignore-unmatch $FILE_PATH" \
--prune-empty --tag-name-filter cat -- --all
```

### Push changes to repository
```
git push origin --force --all
git push origin --force --tags
```

### Enable Git LFS
```
sudo apt-get install git-lfs
git lfs install
git lfs track "*.tar.gz"
```

### Articles
* [Removing sensitive data from a repository](https://help.github.com/articles/removing-sensitive-data-from-a-repository/)
* [How to find/identify large files/commits in Git history?](https://stackoverflow.com/questions/10622179/how-to-find-identify-large-files-commits-in-git-history)
* [How to remove/delete a large file from commit history in Git repository?](https://stackoverflow.com/questions/2100907/how-to-remove-delete-a-large-file-from-commit-history-in-git-repository)
* [Git LFS](https://docs.gitlab.com/ee/workflow/lfs/manage_large_binaries_with_git_lfs.html)
