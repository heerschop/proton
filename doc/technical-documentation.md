# Proton technical documentation

### Shebang
* Environment variable expansion
* Auto script directory prepend
* Directory slash translation
* Extension specification
* Evaluation review (--show actions)

### Shell Selection
* Build in fallback shell
* Shell environment variables

### Debug configuration the (Show argument)
* schema
* elements
* references
* environment
* actions
* hierarchy

### Configuration keywords
* settings
* template
    SCRIPT_ROOT
    GOOS
```
proton config.sh --show hierarchy --filter "^templates.init"
```

* environment
    SHELL
    SHELL_ARGS
    SCRIPT_ROOT
    GOOS
* init
* final
* component
* latent
* term
* detect
* apply
* remove

### Configuration structure / hierarchy
* <<  =< ==
* Global
* Category
* Action
* References

### Configuration filter query
* regular expresions
* hierarchy filters
