# Previous Releases

| Version                                                                    | Date         |
|:-------------------------------------------------------------------------- |:------------ |
| [2.0.0-rc.5](https://gitlab.com/heerschop/proton/blob/2.0.0-rc.5/README.md)| 22-12-2018   |
| [2.0.0-rc.4](https://gitlab.com/heerschop/proton/blob/2.0.0-rc.4/README.md)| 02-12-2018   |
| [2.0.0-rc.3](https://gitlab.com/heerschop/proton/blob/2.0.0-rc.3/README.md)| 29-11-2018   |
| [2.0.0-rc.2](https://gitlab.com/heerschop/proton/blob/2.0.0-rc.2/README.md)| 20-11-2018   |
| [2.0.0-rc.1](https://gitlab.com/heerschop/proton/blob/2.0.0-rc.1/README.md)| 14-11-2018   |
| [2.0.0-rc.0](https://gitlab.com/heerschop/proton/blob/2.0.0-rc.0/README.md)| 11-11-2018   |
| [1.0.0](https://gitlab.com/heerschop/proton/blob/1.0.0/readme.md)          | 01-05-2017   |
| [1.0.0-rc.3](https://gitlab.com/heerschop/proton/blob/1.0.0-rc.3/readme.md)| 03-04-2017   |
| [1.0.0-rc.2](https://gitlab.com/heerschop/proton/blob/1.0.0-rc.2/readme.md)| 02-04-2017   |
| [1.0.0-rc.1](https://gitlab.com/heerschop/proton/blob/1.0.0-rc.1/readme.md)| 02-04-2017   |
| [1.0.0-rc.0](https://gitlab.com/heerschop/proton/blob/1.0.0-rc.0/readme.md)| 01-04-2017   |
| [0.2.4](https://gitlab.com/heerschop/proton/blob/0.2.4/readme.md)          | 01-04-2017   |
| [0.2.0](https://gitlab.com/heerschop/proton/blob/0.2.0/readme.md)          | 31-03-2017   |
| [0.0.1](https://gitlab.com/heerschop/proton/blob/0.0.1/readme.md)          | 30-03-2017   |
